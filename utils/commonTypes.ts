import { SetStateAction, Dispatch, ReactNode } from 'react'
import { GestureResponderEvent } from 'react-native'

export type DateRange = {
  startDate: MomentDate
  endDate?: MomentDate
}

export type TransactionRange = {
  min?: number
  max?: number
}

export type RouteEntry = {
  name: string
  component: any
}

export type OnPressCallback = (e: GestureResponderEvent) => void

export type optionProps = {
  id: any
  name: string
}

export type MomentDate = Date | moment.Moment | string

export type StateCallback<t> = (value: t) => void | Dispatch<SetStateAction<t>>

export type NumChar = number | string

export type ReactChild = { children?: ReactNode }
