import moment from 'moment'

import { ExpenseType } from '../data/constants/expense'
import { ExpenseProcess } from '../data/interface/expense'
import { getAsyncData, setAsyncData } from './asyncStorage'
import { NumChar } from './commonTypes'

// -----------------------------------------------------------------------

export type CurrencyProps = (
  cur: number | string,
  config?: {
    type?: 'INR' | 'USD' | string
    zeroOnNull?: boolean
  },
) => string | null
export const currency: CurrencyProps = (cur, config) => {
  const { type = 'INR', zeroOnNull = true } = config || {}
  const curType = type !== 'INR' ? 'en-US' : 'en-IN'

  if (cur || cur === 0) {
    const curConv = convertToInt(cur)
    return Intl.NumberFormat(curType, {
      style: 'currency',
      currency: type,
    }).format(curConv)
  }

  if (zeroOnNull) {
    return Intl.NumberFormat(curType, {
      style: 'currency',
      currency: type,
    }).format(0)
  }

  return null
}

// -----------------------------------------------------------------------

export type NumberFmtProps = (
  cur: number | string,
  config?: {
    type?: 'IN' | 'US' | string
    zeroOnNull?: boolean
    activeChange?: boolean // enable if it is a active change like textfield
  },
) => string | null

export const numberFmt: NumberFmtProps = (num, config) => {
  const { type = 'IN', zeroOnNull, activeChange } = config || {}
  const fmtType = type !== 'IN' ? 'en-US' : 'en-IN'

  const intNum = convertToInt(num)

  if (intNum) {
    const converted = intNum.toLocaleString(fmtType)

    if (activeChange && `${num}`.charAt((`${num}`?.length || 1) - 1) === '.') {
      return `${converted}.`
    }

    return converted
  }

  return zeroOnNull ? '0' : ''
}

// -----------------------------------------------------------------------

export const dateFmt = (value: Date | unknown, format?: string | boolean) => {
  if (moment(value).isValid()) {
    const isTime = typeof format === 'boolean' && !!format

    return moment(value).format(
      isTime ? 'DD MMM YYYY, hh:mm A' : (format as string) || 'DD MMM YYYY',
    )
  }
  return ''
}

// -----------------------------------------------------------------------

export const getEnv = (value?: string): string => {
  if (value) {
    return process.env[value] as string
  }
  return undefined
}

// -----------------------------------------------------------------------

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const logError = async (message: any) => {
  const shouldLog = !!(await getAsyncData('shouldLogErrors'))

  if (__DEV__) {
    console.error(message)
  } else if (shouldLog && !__DEV__) {
    await setAsyncData('errorLogs', [JSON.stringify(message)], true)
  }
}

// -----------------------------------------------------------------------

export const nullify = (value: any): any => {
  return Object.keys(value || {}).reduce((a, key: any) => {
    const curValue = value[key]
    return {
      ...a,
      [key]: curValue || curValue === 0 || curValue === false ? curValue : null,
    }
  }, {})
}

// -----------------------------------------------------------------------

export const calcTransaction = (
  balance: NumChar,
  transaction: NumChar,
  type: ExpenseProcess,
) => {
  const calcBal = convertToInt(balance)
  const calcTrx = convertToInt(transaction)

  if (type === ExpenseType.income) {
    return calcBal + calcTrx
  }
  return calcBal - calcTrx
}

// -----------------------------------------------------------------------

// prettier-ignore
export const updateExistingTransaction = (
  balance: NumChar,
  newTransaction: NumChar,
  oldTransaction: NumChar,
  type: ExpenseProcess,
) => {
  const calcBal = convertToInt(balance)
  const newTran = convertToInt(newTransaction)
  const oldTran = convertToInt(oldTransaction)

  const transaction = newTran - oldTran

  if (type === ExpenseType.income) {
    return calcBal + transaction
  }
  return calcBal - transaction
}

// -----------------------------------------------------------------------

export const genUUID = () => {
  const timestamp = Math.floor(new Date().getTime() / 1000).toString(16)
  return (
    timestamp +
    'xxxxxxxxxxxxxxxx'
      .replace(/[x]/g, () => {
        return Math.floor(Math.random() * 16).toString(16)
      })
      .toLowerCase()
  )
}

// -----------------------------------------------------------------------

export const convertToInt = (value: NumChar) => {
  if (typeof value === 'string') return parseFloat(value)
  if (typeof value === 'number') return value
  return 0
}
