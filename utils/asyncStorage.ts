import AsyncStorage from '@react-native-async-storage/async-storage'
import { logError } from './commonFunctions'

export const setAsyncData = async (
  name: string,
  value: any,
  isAppend?: boolean,
) => {
  try {
    let storageValue

    if (typeof value === 'string') {
      storageValue = value
    } else if (typeof value === 'object' && isAppend) {
      const existingData = await getAsyncData(name)

      if (Array.isArray(value)) {
        storageValue = JSON.stringify([...(existingData || []), ...value])
      } else {
        storageValue = JSON.stringify({ ...(existingData || {}), ...value })
      }
    } else {
      storageValue = JSON.stringify(value)
    }

    return await AsyncStorage.setItem(`@${name}`, storageValue)
  } catch (e) {
    await logError(e)
  }

  return null
}

export const getAsyncData = async (name: string, getRaw?: boolean) => {
  try {
    const value = await AsyncStorage.getItem(`@${name}`)
    if (value !== null) {
      return getRaw ? value : JSON.parse(value)
    }
  } catch (e) {
    logError(e)
  }

  return null
}

export const removeAsyncData = async (name: string) => {
  try {
    await AsyncStorage.removeItem(`@${name}`)
    return true
  } catch (e) {
    logError(e)
    return false
  }
}

// -----------------------------------------------------------------------
