/* eslint-disable @typescript-eslint/no-var-requires */
const { getDefaultConfig } = require('expo/metro-config')

const config = getDefaultConfig(__dirname)

config.resolver.assetExts.push('db')
config.resolver.assetExts.push('cjs')
config.transformer.minifierConfig = {
  keep_classnames: true,
  keep_fnames: true,
  mangle: {
    // toplevel: false,
    keep_classnames: true,
    keep_fnames: true, // FIX typeorm
  },
  output: {
    ascii_only: true,
    quote_style: 3,
    wrap_iife: true,
  },
  sourceMap: {
    includeSources: false,
  },
  toplevel: false,
  compress: {
    // reduce_funcs inlines single-use functions, which cause perf regressions.
    reduce_funcs: false,
  },
}

module.exports = config
