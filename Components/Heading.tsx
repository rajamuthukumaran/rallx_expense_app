import React from 'react'
import { Box } from 'native-base'
import { Text } from './Input'
import { TextProps } from './Input/Text'
import { fonts } from '../mixins'

export type HeadingProps = {
  label?: string
} & TextProps

const Heading: React.FC<HeadingProps> = ({
  children,
  label,
  ...rest
}): React.ReactElement => {
  return (
    <Box>
      <Text fontSize="3xl" fontFamily={fonts.Montserrat500} {...rest}>
        {children || label}
      </Text>
    </Box>
  )
}

export default Heading
