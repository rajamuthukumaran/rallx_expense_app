import React, { useMemo } from 'react'
import TextField, { TextFieldProps } from './TextField'
import { numberFmt } from '../../utils/commonFunctions'

export type NumberFieldProps = TextFieldProps & { isCurrency?: boolean }

const NumberField: React.FC<NumberFieldProps> = ({
  isCurrency,
  onChangeText,
  value,
  ...rest
}) => {
  const formattedValue = useMemo(() => {
    const fmtValue = numberFmt(value, {
      activeChange: true,
    })

    if (isCurrency && value) {
      return `₹ ${fmtValue}`
    }

    return fmtValue
  }, [isCurrency, value])

  const handleChange = (text: string) => {
    const refineValue = text?.match(/[+-]?((\d+\.?\d*)|(\.\d+))/g)?.join('') // remove , and other formatting string
    onChangeText(refineValue || '')
  }

  return (
    <TextField value={formattedValue} onChangeText={handleChange} {...rest} />
  )
}

export default NumberField
