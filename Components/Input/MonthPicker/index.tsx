import React, { useState } from 'react'
import { Box, Center, Popover, Pressable } from 'native-base'
import moment from 'moment'
// import { FlingGestureHandler, Directions } from 'react-native-gesture-handler'
import { Text } from '..'
import { months } from './constant'
import { MomentDate, NumChar, StateCallback } from '../../../utils/commonTypes'

export type MonthPickerProps = {
  value: MomentDate
  onChange: StateCallback<MomentDate>
}

type MonthTriggerProps = {
  triggerProps: any
  handleOpen: () => void
  value: MomentDate
} & Omit<MonthPickerProps, 'value' | 'onChange'>

const MonthPicker: React.FC<MonthPickerProps> = ({
  value,
  onChange,
  ...rest
}) => {
  const [isOpen, setOpen] = useState(false)

  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)

  // const handleYear = (year: NumChar) => {
  //   const currentMonth = moment(value).format('m')
  //   if (onChange) onChange(moment(`${currentMonth} ${year}`).toDate())
  // }

  const handleMonth = (month: NumChar) => {
    const currentYear = moment(value).format('y')
    if (onChange) {
      onChange(moment(`15 ${month} ${currentYear}`, 'DD MMMM YYYY'))
    }
    handleClose()
  }

  return (
    <Popover
      isOpen={isOpen}
      onClose={handleClose}
      trigger={triggerProps =>
        MonthTrigger({ triggerProps, handleOpen, value, ...rest })
      }
    >
      <Popover.Content accessibilityLabel="month_picker" borderRadius="xl">
        <Popover.Body>
          <Box>
            {/* <Select /> */}
            <Box flexDirection="row" flexWrap="wrap">
              {months.map(month => (
                <Pressable
                  key={month.id}
                  onPress={() => handleMonth(month.name)}
                >
                  <Center size="16">{month.short}</Center>
                </Pressable>
              ))}
            </Box>
          </Box>
        </Popover.Body>
      </Popover.Content>
    </Popover>
  )
}

const MonthTrigger = ({
  triggerProps,
  handleOpen,
  value,
  ...rest
}: MonthTriggerProps) => (
  <Box>
    <Pressable {...triggerProps} onPress={handleOpen}>
      <Text fontSize="2xl" {...rest}>
        {moment(value).format('MMMM YY')}
      </Text>
    </Pressable>
  </Box>
)

export default MonthPicker
