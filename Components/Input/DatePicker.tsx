import React, { useState } from 'react'
import { Icon, Pressable } from 'native-base'
import DateTimePicker, {
  DatePickerOptions,
  DateTimePickerAndroid,
} from '@react-native-community/datetimepicker'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { Platform } from 'react-native'
import TextField, { TextFieldProps } from './TextField'
import { dateFmt, logError } from '../../utils/commonFunctions'

export type DatePickerProps = {
  value?: Date
  mode?: 'date' | 'time'
  display?: 'default' | 'inline' | 'compact' | 'spinner' | 'clock' | 'calendar'
  onChange?: (date: Date) => void
  datePickerProps?: DatePickerOptions
} & TextFieldProps

export const DatePicker: React.FC<DatePickerProps> = ({
  value,
  onChange,
  mode,
  display,
  datePickerProps,
  disabled,
  isDisabled,
  ...rest
}) => {
  const [showPicker, setShowPicker] = useState(false)

  const whenPressable = !(isDisabled || disabled) && !showPicker
  const isAndroid = Platform.OS === 'android'

  const handleChange = (e: any, date: Date) => {
    if (onChange && date) onChange(date)
    setShowPicker(false)
  }

  const handleOpen = () => {
    if (isAndroid) {
      DateTimePickerAndroid.open({
        value: value || new Date(),
        onChange: handleChange,
        mode: 'date',
        onError: err => logError(err),
      })
    } else if (whenPressable) {
      setShowPicker(true)
    }
  }

  return (
    <>
      <Pressable onPress={handleOpen}>
        <TextField
          isReadOnly
          InputRightElement={
            <Icon
              as={<MaterialCommunityIcons name="calendar" />}
              size={5}
              mr="2"
              color="muted.400"
            />
          }
          disabled={isDisabled || disabled}
          {...rest}
          value={dateFmt(value)}
        />
      </Pressable>
      {!!showPicker && (
        <DateTimePicker
          disabled={isDisabled || disabled}
          value={value || new Date()}
          mode={mode}
          display={(display as any) || 'default'}
          onChange={handleChange}
          {...datePickerProps}
        />
      )}
    </>
  )
}

export default DatePicker
