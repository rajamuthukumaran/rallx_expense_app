import React from 'react'
import { TextArea as NBTextArea } from 'native-base'
import { ITextAreaProps } from 'native-base/lib/typescript/components/primitives/TextArea'
import InputWrapper, { CommonInputProps } from './InputWrapper'

export type TextAreaProps = {
  disabled?: boolean
} & ITextAreaProps &
  CommonInputProps

const TextArea: React.FC<TextAreaProps> = ({
  error,
  label,
  labelProps,
  errorProps,
  containerProps,
  isRequired,
  isDisabled,
  disabled,
  ...rest
}) => {
  const commonProps = {
    error,
    label,
    labelProps,
    errorProps,
    containerProps,
    isRequired,
  }

  return (
    <InputWrapper {...commonProps}>
      <NBTextArea
        h={20}
        w={{
          base: '100%',
          md: '25%',
        }}
        isDisabled={isDisabled || disabled}
        {...rest}
      />
    </InputWrapper>
  )
}

export default TextArea
