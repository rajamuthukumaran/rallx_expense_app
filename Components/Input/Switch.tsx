import React from 'react'
import { Switch as NBSwitch, ISwitchProps } from 'native-base'

export type SwitchProps = Omit<ISwitchProps, 'value'>

const Switch: React.FC<SwitchProps> = props => {
  return <NBSwitch {...props} />
}

export default Switch
