import React from 'react'
import {
  Box,
  FormControl,
  IBoxProps,
  IFormControlErrorMessageProps,
} from 'native-base'
import { Text } from '.'
import { TextProps } from './Text'

export type CommonInputProps = {
  labelProps?: TextProps
  errorProps?: IFormControlErrorMessageProps
  containerProps?: IBoxProps
  label?: string
  error?: string
  isRequired?: boolean
}

export const InputWrapper: React.FC<CommonInputProps> = ({
  children,
  containerProps,
  error,
  errorProps,
  isRequired,
  label,
  labelProps,
}) => {
  return (
    <Box {...containerProps}>
      <FormControl isRequired={isRequired} isInvalid={!!error}>
        {!!label && (
          <FormControl.Label>
            <Text {...labelProps}>{label}</Text>{' '}
          </FormControl.Label>
        )}

        {children}

        {!!error && (
          <FormControl.ErrorMessage {...errorProps}>
            {error}
          </FormControl.ErrorMessage>
        )}
      </FormControl>
    </Box>
  )
}

export default InputWrapper
