import React from 'react'
import { ISelectProps, Select as NBSelect } from 'native-base'
import { get } from 'lodash'
import { optionProps } from '../../utils/commonTypes'
import { CommonInputProps, InputWrapper } from './InputWrapper'

export type SelectProps = {
  options?: optionProps[] | any[]
  optionKey?: {
    value?: string
    id?: string
  }
} & ISelectProps &
  CommonInputProps

export const Select: React.FC<SelectProps> = ({
  options,
  optionKey,
  isDisabled,
  error,
  label,
  labelProps,
  errorProps,
  containerProps,
  isRequired,
  ...rest
}) => {
  const labelKey = optionKey?.value || 'name'
  const valueKey = optionKey?.id || 'id'

  const commonProps = {
    error,
    label,
    labelProps,
    errorProps,
    containerProps,
    isRequired,
  }

  return (
    <InputWrapper {...commonProps}>
      <NBSelect isDisabled={isDisabled || !options?.length} {...rest}>
        {options?.map((entry: any) => {
          const entryValue = get(entry, valueKey)
          const entryLabel = get(entry, labelKey)

          return (
            <NBSelect.Item
              key={entryValue}
              label={entryLabel}
              value={entryValue}
            />
          )
        })}
      </NBSelect>
    </InputWrapper>
  )
}

export default Select
