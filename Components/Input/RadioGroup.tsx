import React from 'react'
import { IRadioGroupProps, Radio, IRadioProps, Stack } from 'native-base'
import InputWrapper, { CommonInputProps } from './InputWrapper'

export type RadioOption = IRadioProps & {
  label?: string
}

export type RadioGroupProps = {
  options: RadioOption[]
  disabled?: boolean
  isDisabled?: boolean
} & IRadioGroupProps &
  CommonInputProps

const RadioGroup: React.FC<RadioGroupProps> = ({
  error,
  label,
  labelProps,
  errorProps,
  containerProps,
  isRequired,
  options,
  disabled,
  isDisabled,
  ...rest
}) => {
  const commonProps = {
    error,
    label,
    labelProps,
    errorProps,
    containerProps,
    isRequired,
  }

  return (
    <InputWrapper {...commonProps}>
      <Radio.Group {...rest}>
        <Stack
          ml="-1"
          direction={{
            base: 'row',
          }}
          alignItems={{
            base: 'flex-start',
            md: 'center',
          }}
          space={3}
          w="60%"
          maxW="300px"
        >
          {options?.map(option => (
            <Radio
              key={option.value}
              isDisabled={option.isDisabled || disabled || isDisabled}
              value={option.value}
              m={1}
              {...option}
            >
              {option.label}
            </Radio>
          ))}
        </Stack>
      </Radio.Group>
    </InputWrapper>
  )
}

export default RadioGroup
