import React from 'react'
import { Checkbox as NBCheckbox, ICheckboxProps } from 'native-base'

export type CheckboxProps = Omit<ICheckboxProps, 'value'>

const Checkbox: React.FC<CheckboxProps> = props => {
  return <NBCheckbox value="" {...props} />
}

export default Checkbox
