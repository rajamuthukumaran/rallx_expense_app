import React from 'react'
import { ITextProps, Text as NBText } from 'native-base'

export type TextProps = ITextProps & { dark?: boolean }

export const Text: React.FC<TextProps> = ({ children, dark, ...rest }) => {
  return (
    <NBText color={dark ? 'black.100' : 'white.100'} {...rest}>
      {children}
    </NBText>
  )
}

export default Text
