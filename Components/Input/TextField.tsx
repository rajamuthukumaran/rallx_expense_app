import React from 'react'
import { Input, IInputProps } from 'native-base'
import { CommonInputProps, InputWrapper } from './InputWrapper'

export type TextFieldProps = {
  secureTextEntry?: boolean
  keyboardType?: 'numeric' | string
  disabled?: boolean
} & IInputProps &
  CommonInputProps

const TextField: React.FC<TextFieldProps> = ({
  error,
  label,
  labelProps,
  errorProps,
  containerProps,
  isRequired,
  isDisabled,
  disabled,
  ...rest
}) => {
  const commonProps = {
    error,
    label,
    labelProps,
    errorProps,
    containerProps,
    isRequired,
  }

  return (
    <InputWrapper {...commonProps}>
      <Input isDisabled={isDisabled || disabled} {...rest} />
    </InputWrapper>
  )
}

export default TextField
