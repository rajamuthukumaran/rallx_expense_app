import { IBoxProps, IButtonProps } from 'native-base'
import { OnPressCallback } from '../../../utils/commonTypes'

export type CommonButtonProps = {
  onPress?: OnPressCallback
}

export type AddButtonProps = {
  name?: string
  size?: number
  color?: string
} & CommonButtonProps &
  IBoxProps

export type CustomButtonProps = {
  disabled?: boolean
  isDisabled?: boolean
} & CommonButtonProps &
  IBoxProps

export type RenderCustomTrigger = (
  onPress: () => void,
  trigger: any,
) => JSX.Element

export type ConfirmationButtonProps = {
  msg?: string
  onCancel?: OnPressCallback
  onApply?: OnPressCallback
  label?: string
  render?: RenderCustomTrigger
} & CommonButtonProps &
  IButtonProps

export type ConfirmationTriggerProps = {
  trigger: any
  onPress: () => void
  label?: string
  render?: RenderCustomTrigger
} & IButtonProps
