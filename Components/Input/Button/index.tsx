/* eslint-disable react/no-unstable-nested-components */
import React, { useState } from 'react'
import { Box, Button as NBbutton, Popover } from 'native-base'
import { Pressable } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons'
import {
  AddButtonProps,
  ConfirmationButtonProps,
  ConfirmationTriggerProps,
  CustomButtonProps,
} from './types'
import { colors } from '../../../config/theme'
import { Text } from '..'

export const Button = NBbutton

export const RoundButton: React.FC<AddButtonProps> = ({
  children,
  name,
  size,
  color,
  onPress,
  ...rest
}) => {
  return (
    <Pressable onPress={onPress}>
      <Box bg="primary.200" borderRadius={50} {...rest}>
        {children || (
          <MaterialIcons
            name={(name as any) || 'add'}
            size={size ?? 36}
            color={color || colors.textW}
          />
        )}
      </Box>
    </Pressable>
  )
}

// -----------------------------------------------------------------------

export const CustomButton: React.FC<CustomButtonProps> = ({
  children,
  onPress,
  disabled,
  isDisabled,
  ...rest
}) => (
  <Pressable disabled={disabled || isDisabled} onPress={onPress}>
    <Box {...rest}>{children}</Box>
  </Pressable>
)

// -----------------------------------------------------------------------

const ConfirmationTrigger: React.FC<ConfirmationTriggerProps> = React.memo(
  ({
    trigger,
    children,
    onPress,
    label,
    isDisabled,
    disabled,
    render,
    ...rest
  }) => {
    if (render) return render(onPress, trigger)

    return (
      <NBbutton
        {...trigger}
        onPress={onPress}
        isDisabled={disabled || isDisabled}
        {...rest}
      >
        {label || children}
      </NBbutton>
    )
  },
)

export const ConfirmationButton: React.FC<ConfirmationButtonProps> = ({
  msg,
  onCancel,
  onApply,
  onPress,
  ...rest
}) => {
  const [isOpen, setOpen] = useState(false)
  const defaultMsg = 'Are you sure?'

  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)
  const handleSelection = (e, isApply?: boolean) => {
    if (isApply) {
      const fn = onApply || onPress
      if (fn) fn(e)
    } else if (onCancel) {
      onCancel(e)
    }
    handleClose()
  }

  return (
    <Popover
      isOpen={isOpen}
      onClose={handleClose}
      trigger={triggerProps => (
        <ConfirmationTrigger
          trigger={triggerProps}
          onPress={handleOpen}
          {...rest}
        />
      )}
    >
      <Popover.Content
        accessibilityLabel="confirmation_button"
        borderRadius="xl"
      >
        <Popover.Arrow />
        <Popover.Body>
          <Text textAlign="center">{msg || defaultMsg}</Text>
        </Popover.Body>
        <Popover.Footer justifyContent="center">
          <NBbutton.Group justifyContent="space-between">
            <CustomButton mx={5} onPress={handleSelection}>
              <MaterialIcons name="close" size={30} color="#dc2626" />
            </CustomButton>
            <CustomButton mx={5} onPress={e => handleSelection(e, true)}>
              <MaterialIcons name="done" size={30} color="#16a34a" />
            </CustomButton>
          </NBbutton.Group>
        </Popover.Footer>
      </Popover.Content>
    </Popover>
  )
}

// ---------------------------------------------------------------------------
