import React from 'react'
import { extendTheme, NativeBaseProvider } from 'native-base'

import { palatte } from '../config/theme'
import { ReactChild } from '../utils/commonTypes'

const ThemeProvider: React.FC<ReactChild> = ({ children }) => {
  const themeConfig = extendTheme({
    colors: palatte,
    fontConfig: {
      Poppins: {
        100: {
          normal: 'Poppins100',
          italic: 'Poppins100I',
        },
        200: {
          normal: 'Poppins200',
          italic: 'Poppins200I',
        },
        300: {
          normal: 'Poppins300',
          italic: 'Poppins300I',
        },
        400: {
          normal: 'Poppins400',
          italic: 'Poppins400I',
        },
        500: {
          normal: 'Poppins500',
          italic: 'Poppins500I',
        },
        600: {
          normal: 'Poppins600',
          italic: 'Poppins600I',
        },
        700: {
          normal: 'Poppins700',
          italic: 'Poppins700I',
        },
        800: {
          normal: 'Poppins800',
          italic: 'Poppins800I',
        },
        900: {
          normal: 'Poppins900',
          italic: 'Poppins900I',
        },
      },
      Montserrat: {
        100: {
          normal: 'Montserrat100',
          italic: 'Montserrat100I',
        },
        200: {
          normal: 'Montserrat200',
          italic: 'Montserrat200I',
        },
        300: {
          normal: 'Montserrat300',
          italic: 'Montserrat300I',
        },
        400: {
          normal: 'Montserrat400',
          italic: 'Montserrat400I',
        },
        500: {
          normal: 'Montserrat500',
          italic: 'Montserrat500I',
        },
        600: {
          normal: 'Montserrat600',
          italic: 'Montserrat600I',
        },
        700: {
          normal: 'Montserrat700',
          italic: 'Montserrat700I',
        },
        800: {
          normal: 'Montserrat800',
          italic: 'Montserrat800I',
        },
        900: {
          normal: 'Montserrat900',
          italic: 'Montserrat900I',
        },
      },
    },
    fonts: {
      heading: 'Montserrat',
      body: 'Poppins',
      mono: 'Poppins',
    },
    config: {
      initialColorMode: 'dark',
    },
  })

  return <NativeBaseProvider theme={themeConfig}>{children}</NativeBaseProvider>
}

export default ThemeProvider
