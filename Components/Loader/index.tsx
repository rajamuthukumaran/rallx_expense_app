import React from 'react'
import { Box } from 'native-base'
import { Text } from '../Input'
import { layout } from '../../mixins'

export const LoadingText: React.FC = () => {
  return (
    <Box style={layout.center} py="5">
      <Text>Loading...</Text>
    </Box>
  )
}
