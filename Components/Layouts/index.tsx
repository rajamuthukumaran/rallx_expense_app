import React from 'react'
import { Box, IBoxProps, IconButton, ScrollView } from 'native-base'
import { MaterialIcons } from '@expo/vector-icons'
import { useNavigation } from '@react-navigation/native'
import { OnPressCallback } from '../../utils/commonTypes'
import { CustomButton } from '../Input/Button'
import { colors } from '../../config/theme'
import { layout } from '../../mixins'
import Heading from '../Heading'

export type CommonContainerProps = {
  hasPartiton?: boolean
  withoutPadding?: boolean
} & IBoxProps

export const ScreenContainer: React.FC<CommonContainerProps> = ({
  children,
  hasPartiton,
  withoutPadding,
  ...rest
}) => {
  return (
    <Box
      flex={1}
      bg="black.100"
      justifyContent={hasPartiton ? 'space-between' : undefined}
      {...rest}
      px={withoutPadding ? 0 : 5}
      pb={withoutPadding ? 0 : 5}
      pt={withoutPadding ? 0 : 3}
    >
      {children}
    </Box>
  )
}

// ----------------------------------------------------------------------------

export type ModelContainerProps = {
  onClose?: OnPressCallback
  action?: any
  actionProps?: IBoxProps
  label?: string
} & CommonContainerProps

export const ModelContainer: React.FC<ModelContainerProps> = ({
  children,
  onClose,
  withoutPadding,
  hasPartiton,
  action,
  actionProps,
  label,
  ...rest
}) => {
  const navigatior = useNavigation()

  const handleClose = () => {
    navigatior.goBack()
  }

  return (
    <ScreenContainer withoutPadding>
      <Box pr={3} pl={5} py={3} style={layout.lrPartition} mb={label ? 2 : 0}>
        <Box>
          <Heading label={label} fontSize="2xl" />
        </Box>
        <Box alignItems="flex-end">
          <CustomButton onPress={onClose || handleClose}>
            <MaterialIcons name="close" size={32} color={colors.textW} />
          </CustomButton>
        </Box>
      </Box>
      <Box
        flex={1}
        px={withoutPadding ? 0 : 5}
        pb={withoutPadding ? 0 : 5}
        justifyContent={hasPartiton || !!action ? 'space-between' : undefined}
        {...rest}
      >
        {action ? (
          <>
            <ScrollView>
              <Box>{children}</Box>
            </ScrollView>
            <Box pt={5} {...actionProps}>
              {action}
            </Box>
          </>
        ) : (
          children
        )}
      </Box>
    </ScreenContainer>
  )
}

// -----------------------------------------------------------------------

export type ScreenModelContainerProps = Omit<ModelContainerProps, 'onClose'> & {
  onBack?: OnPressCallback
  titleCompanion?: any
  titleCompanionProps?: IBoxProps
}

export const ScreenModelContainer: React.FC<ScreenModelContainerProps> = ({
  children,
  onBack,
  withoutPadding,
  hasPartiton,
  action,
  actionProps,
  label,
  titleCompanion,
  titleCompanionProps,
  ...rest
}) => {
  const navigatior = useNavigation()

  const handleClose = () => {
    navigatior.goBack()
  }

  return (
    <ScreenContainer withoutPadding>
      <Box pr={3} pl={5} py={3} style={layout.lrPartition} mb={label ? 2 : 0}>
        <Box style={layout.asideRow}>
          <Box ml="-3" mt="0.5">
            <IconButton
              variant="ghost"
              _icon={{
                as: MaterialIcons,
                name: 'arrow-back',
                color: 'white.100',
                size: 6,
              }}
              borderRadius={1000}
              onPress={onBack || handleClose}
            />
          </Box>
          <Heading label={label} fontSize="2xl" ml="2" />
        </Box>

        {!!titleCompanion && (
          <Box {...(titleCompanionProps || {})}>{titleCompanion}</Box>
        )}
      </Box>
      <Box
        flex={1}
        px={withoutPadding ? 0 : 5}
        pb={withoutPadding ? 0 : 5}
        justifyContent={hasPartiton || !!action ? 'space-between' : undefined}
        {...rest}
      >
        {action ? (
          <>
            <ScrollView>
              <Box>{children}</Box>
            </ScrollView>
            <Box pt={5} {...actionProps}>
              {action}
            </Box>
          </>
        ) : (
          children
        )}
      </Box>
    </ScreenContainer>
  )
}
