/* eslint-disable react/jsx-no-useless-fragment */
import React from 'react'
import { ReactChild } from '../utils/commonTypes'

export type DecisionProps = {
  when: boolean
}

export const Show: React.FC<DecisionProps & ReactChild> = ({
  when,
  children,
}) => when && <>{children}</>
