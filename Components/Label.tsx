import React from 'react'
import { Box, IBoxProps, Text } from 'native-base'
import { layout } from '../mixins'

export type LabelProps = {
  value?: string
  isRequired?: boolean
} & IBoxProps

const Label: React.FC<LabelProps> = ({
  children,
  value,
  isRequired,
  fontFamily,
  fontSize,
  fontStyle,
  fontWeight,
  color,
  ...rest
}) => {
  const allyProps = {
    fontFamily,
    fontSize,
    fontStyle,
    fontWeight,
  }

  return (
    <Box style={[layout.asideRow]} {...rest}>
      <Text color={color} {...allyProps}>
        {value || children}
      </Text>
      {isRequired && (
        <Text color="red.200" {...allyProps}>
          {' '}
          *
        </Text>
      )}
    </Box>
  )
}

export default Label
