import { get } from 'lodash'
import React from 'react'
import {
  Control,
  Controller,
  FieldErrors,
  RegisterOptions,
} from 'react-hook-form'
import { NumberField, TextField } from '../Input'
import Checkbox, { CheckboxProps } from '../Input/Checkbox'
import DatePicker, { DatePickerProps } from '../Input/DatePicker'
import { NumberFieldProps } from '../Input/NumberField'
import RadioGroup, { RadioGroupProps } from '../Input/RadioGroup'
import { SelectProps, Select } from '../Input/Select'
import TextArea, { TextAreaProps } from '../Input/TextArea'
import { TextFieldProps } from '../Input/TextField'

export type FormFieldCommonProps<T = any> = {
  name: string
  control: Control<T>
  defaultValue?: any
  rules?: Exclude<
    RegisterOptions,
    'valueAsNumber' | 'valueAsDate' | 'setValueAs'
  >
  errors?: FieldErrors<T>
  isRequired?: boolean
}

// ----------------- Text field ------------------------------------------

export type FormTextFieldProps = FormFieldCommonProps & TextFieldProps

export const FormTextField: React.FC<FormTextFieldProps> = ({
  name,
  control,
  defaultValue,
  rules = {},
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value, onBlur } }) => (
        <TextField
          {...rest}
          onBlur={onBlur}
          value={value}
          onChangeText={(e: any) => onChange(e)}
          error={get(errors, `${name}.message`)}
        />
      )}
    />
  )
}

// ----------------- Number field ------------------------------------------

export type FormNumberFieldProps = FormFieldCommonProps & NumberFieldProps

export const FormNumberField: React.FC<FormNumberFieldProps> = ({
  name,
  control,
  defaultValue,
  rules = {},
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value, onBlur } }) => (
        <NumberField
          {...rest}
          onBlur={onBlur}
          value={value}
          onChangeText={(e: any) => onChange(e)}
          error={get(errors, `${name}.message`)}
        />
      )}
    />
  )
}

// -------------------- Select ------------------------------------------

export type FormSelectFieldProps = FormFieldCommonProps & SelectProps

export const FormSelectField: React.FC<FormSelectFieldProps> = ({
  name,
  control,
  defaultValue,
  rules,
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value } }) => (
        <Select
          {...rest}
          selectedValue={value}
          onValueChange={(e: any) => onChange(e)}
          error={get(errors, `${name}.message`)}
        />
      )}
    />
  )
}

// -------------------- Date ------------------------------------------

export type FormDatePickerProps = FormFieldCommonProps & DatePickerProps

export const FormDatePicker: React.FC<FormDatePickerProps> = ({
  name,
  control,
  defaultValue,
  rules,
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value } }) => (
        <DatePicker
          {...rest}
          value={value}
          onChange={(e: any) => onChange(e)}
          error={get(errors, `${name}.message`)}
        />
      )}
    />
  )
}

// -------------------- Text Area ---------------------------------------

export type FormTextAreaProps = FormFieldCommonProps & TextAreaProps

export const FormTextArea: React.FC<FormTextFieldProps> = ({
  name,
  control,
  defaultValue,
  rules = {},
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value, onBlur } }) => (
        <TextArea
          {...rest}
          onBlur={onBlur}
          value={value}
          onChangeText={(e: any) => onChange(e)}
          error={get(errors, `${name}.message`)}
        />
      )}
    />
  )
}

// -------------------- Checkbox -------------------------------------

export type FormCheckboxProps = FormFieldCommonProps & CheckboxProps

export const FormCheckbox: React.FC<FormCheckboxProps> = ({
  name,
  control,
  defaultValue,
  rules = {},
  // errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value } }) => (
        <Checkbox
          {...rest}
          isChecked={value}
          onChange={isSelected => onChange(isSelected)}
          // error={errors?.[name]?.message}
        />
      )}
    />
  )
}

// -------------------- Radio group -------------------------------------

export type FormRadioGroupProps = FormFieldCommonProps & RadioGroupProps

export const FormRadioGroup: React.FC<FormRadioGroupProps> = ({
  name,
  control,
  defaultValue,
  rules = {},
  errors,
  ...rest
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required: rest.isRequired ? 'Required' : '', ...rules }}
      render={({ field: { onChange, value } }) => (
        <RadioGroup
          name={name}
          value={value}
          onChange={onChange}
          error={get(errors, `${name}.message`)}
          {...rest}
        />
      )}
    />
  )
}
