import MoneyLoader from './MoneyLoader'

export type FullscreenLoaderProps = { text?: string }

export { MoneyLoader }
