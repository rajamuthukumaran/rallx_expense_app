import React from 'react'
import { View, Image, Text } from 'react-native'
import SafeView from '../SafeView'
import { colors } from '../../config/theme'
import { FullscreenLoaderProps } from '.'

const MoneyLoader: React.FC<FullscreenLoaderProps> = ({ text }) => {
  return (
    <SafeView>
      <View
        style={{
          backgroundColor: colors.bgD,
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Image
          source={require('../../assets/loading.gif')}
          style={{ height: 500, width: 500 }}
        />
        <Text
          style={{
            fontSize: 32,
            color: colors.textW,
          }}
        >
          {text || 'Loading...'}
        </Text>
      </View>
    </SafeView>
  )
}

export default MoneyLoader
