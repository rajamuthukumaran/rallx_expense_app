import React from 'react'
import {
  SafeAreaView,
  View,
  ViewProps,
  Platform,
  StatusBar,
  StyleSheetProperties,
} from 'react-native'
import { colors } from '../config/theme'

export type SafeViewProps = {
  style?: StyleSheetProperties
} & ViewProps

const SafeView: React.FC<SafeViewProps> = ({ children, style, ...rest }) => {
  return (
    <SafeAreaView
      style={{
        paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0,
        flex: 1,
        backgroundColor: '#000',
      }}
    >
      <View style={[style, { flex: 1, backgroundColor: colors.bgD }]} {...rest}>
        {children}
      </View>
    </SafeAreaView>
  )
}

export default SafeView
