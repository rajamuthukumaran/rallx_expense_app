import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react'
import { DataSource } from 'typeorm'
import { AuthObject, useAuth } from '../components/Authendication'
import { MoneyLoader } from '../components/FullscreenLoader'
import { logError } from '../utils/commonFunctions'
import { ReactChild } from '../utils/commonTypes'
import { AccountController } from './controller/account'
import { CategoryController } from './controller/category'
import { ExpenseController } from './controller/expense'
import { LoanController } from './controller/loan'
import { Account } from './entities/account'
import { Category } from './entities/category'
import { Expense } from './entities/expense'
import { Loan } from './entities/loan'

export type DataConn = {
  accountRepo: AccountController
  expenseRepo: ExpenseController
  loanRepo: LoanController
  categoryRepo: CategoryController
  conn: DataSource | null
}

export type RepoData = {
  conn: DataSource
  authInfo: AuthObject
}

const DataConn = React.createContext<DataConn>({} as DataConn)

const DataProvider: React.FC<ReactChild> = ({
  children,
}): React.ReactElement => {
  const [conn, setConn] = useState<DataSource | null>(null)
  const authInfo = useAuth()

  const repoData: RepoData = useMemo(
    () => ({
      conn,
      authInfo,
    }),
    [conn, authInfo],
  )

  const connect = useCallback(async () => {
    try {
      const res = new DataSource({
        type: 'expo',
        database: `rallx_expense.db`,
        // eslint-disable-next-line global-require
        driver: require('expo-sqlite'),
        entities: [Account, Expense, Loan, Category],
        synchronize: true,
        logging: true,
      })

      res
        .initialize()
        .then(() => {
          setConn(res)
        })
        .catch(err => logError(err))
    } catch (e) {
      logError(e)
      setConn(null)
    }
  }, [])

  useEffect(() => {
    if (!conn) {
      connect()
    }

    return () => conn && (conn.destroy() as any)
  }, [conn, connect])

  const repos = useMemo(() => {
    if (conn) {
      return {
        accountRepo: new AccountController(repoData),
        expenseRepo: new ExpenseController(repoData),
        loanRepo: new LoanController(repoData),
        categoryRepo: new CategoryController(repoData),
        conn,
      }
    }

    return undefined
  }, [conn, repoData])

  if (!conn) {
    return <MoneyLoader />
  }

  return <DataConn.Provider value={repos}>{children}</DataConn.Provider>
}

export const useDatabase = () => {
  const conn = useContext(DataConn)

  return conn
}

export default DataProvider
