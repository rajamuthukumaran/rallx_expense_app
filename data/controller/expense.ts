import { isEmpty, omit } from 'lodash'
import moment from 'moment'
import { Repository, Between, DataSource } from 'typeorm'
import { RepoData } from '..'
import { AuthObject } from '../../components/Authendication'
import {
  calcTransaction,
  convertToInt,
  genUUID,
  logError,
  updateExistingTransaction,
} from '../../utils/commonFunctions'
import {
  genPromise,
  Options,
  insertTypeObject,
} from '../../utils/functionCreater'
import { ExpenseType } from '../constants/expense'
import { Account } from '../entities/account'
import { Category } from '../entities/category'
import { Expense } from '../entities/expense'
import { Loan } from '../entities/loan'
import {
  BalancePeriod,
  ExpenseFilter,
  Transaction,
  TransactionBalance,
  UpdateTransaction,
  ExpenseProcess,
} from '../interface/expense'

export class ExpenseController {
  private repo: Repository<Expense>

  private conn: DataSource

  private userInfo: AuthObject

  constructor(args: RepoData) {
    this.repo = args.conn.getRepository(Expense)
    this.conn = args.conn
    this.userInfo = args.authInfo
  }

  // &&&&&&&&&&&&&&&&&&&&&&&& Get Expense &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  getExpense = async (
    options?: Options & { filter?: ExpenseFilter },
  ): Promise<Expense[]> => {
    const { date, account, category, transaction, period, ...otherFilters } =
      options?.filter || {}

    let repoFilter: any = {}

    // Date
    const range = {
      startDate:
        date?.startDate ||
        moment()
          .startOf(period || 'month')
          .toISOString(),
      endDate:
        date?.endDate ||
        moment()
          .endOf(period || 'month')
          .toISOString(),
    }
    repoFilter = {
      ...repoFilter,
      date: Between(range.startDate, range.endDate),
    }

    // Transaction
    if (!isEmpty(transaction)) {
      repoFilter = {
        ...repoFilter,
        transaction: Between(transaction.min, transaction.max),
      }
    }

    // Account
    if (account) {
      const accountFilter = await insertTypeObject(account, this.conn, Account)
      repoFilter = {
        ...repoFilter,
        account: accountFilter,
      }
    }

    // Category
    if (category) {
      const categoryFilter = await insertTypeObject(
        category,
        this.conn,
        Category,
      )
      repoFilter = {
        ...repoFilter,
        category: categoryFilter,
      }
    }

    return genPromise(() =>
      this.repo.find({
        where: {
          user_id: this.userInfo.uid,
          ...repoFilter,
          ...(otherFilters || {}),
        },
        relations: ['account', 'loan', 'category'],
      }),
    )
  }

  // &&&&&&&&&&&&&&&&&&&&&&&& Get Balance &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  getBalance = async (filter?: BalancePeriod): Promise<TransactionBalance> => {
    const { date, includeAllBal, period } = filter || {}

    try {
      // Date
      const range = {
        startDate: moment(date?.startDate)
          .startOf(period || 'month')
          .format('YYYY-MM-DD HH:MM:SS.SSS'),

        endDate: moment(date?.endDate || date?.startDate)
          .endOf(period || 'month')
          .format('YYYY-MM-DD HH:MM:SS.SSS'),
      }

      const res = await (includeAllBal
        ? this.repo
            .createQueryBuilder('exp')
            .select('exp.type', 'type')
            .addSelect(`SUM(exp.transaction)`, 'sum')
            .groupBy('exp.type')
            .where(
              `exp.user_id = :uid AND exp.date > :startDate AND exp.date < :endDate`,
              {
                uid: this.userInfo.uid,
                startDate: range.startDate,
                endDate: range.endDate,
              },
            )
            .getRawMany()
        : this.repo
            .createQueryBuilder('exp')
            .select('exp.type', 'type')
            .addSelect(`SUM(exp.transaction)`, 'sum')
            .groupBy('exp.type')
            .leftJoin('exp.account', 'acc')
            .where(
              `exp.user_id = :uid AND exp.date > :startDate AND exp.date < :endDate AND acc.include_in_bal = :showAllBal`,
              {
                uid: this.userInfo.uid,
                startDate: range.startDate,
                endDate: range.endDate,
                showAllBal: 1,
              },
            )
            .getRawMany())

      return res?.reduce((acc: any, cur: any) => {
        return {
          ...acc,
          [cur.type]: cur.sum,
        }
      }, {})
    } catch (e) {
      logError(e)
      return {
        income: 0,
        expense: 0,
        transfer: 0,
      }
    }
  }

  // &&&&&&&&&&&&&&&&&&&&&&&& Add Expense &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  addExpense = async (data: Transaction, options?: Options) => {
    return genPromise(async () => {
      const { accounts, category, name, description, ...rest } = data || {}

      const categoryObj: Category = await insertTypeObject(
        category,
        this.conn,
        Category,
      )
      const transaction_id = genUUID()

      const accTransactions = accounts?.map(async acc => {
        // Retrieve transfer account
        const transfer_account: Account = await insertTypeObject(
          acc.transfer_account,
          this.conn,
          Account,
        )
        const transfer_loan: Loan = await insertTypeObject(
          acc.transfer_loan,
          this.conn,
          Loan,
        )

        const doesnt_count =
          acc?.doesnt_count ?? transfer_account
            ? transfer_account?.include_in_bal
            : false

        // Common Exp data for loan and account
        const expData = {
          user_id: this.userInfo.uid,
          category: categoryObj,
          type: acc.process,
          transaction: convertToInt(acc.amount),
          transfer_account,
          transfer_loan,
          doesnt_count,
          transaction_id,
          name: `${name}`.trim(),
          description: description ? description?.trim() : description,
          ...rest,
        }

        // ========================= Update if loan =========================================
        if (acc.loan) {
          const loanObj: Loan = await insertTypeObject(
            acc.loan,
            this.conn,
            Loan,
          )

          const expense = this.repo.create({
            ...expData,
            loan: loanObj,
          })

          const addExpense = () => this.repo.save(expense)

          const updateLoan = () =>
            this.conn.getRepository(Loan).update(
              { id: loanObj.id },
              {
                balance: calcTransaction(
                  loanObj.balance,
                  acc.amount,
                  acc.process,
                ),
              },
            )

          return Promise.all([addExpense(), updateLoan()])
        }

        // ========================= Update if account =========================================
        const accObj: Account = await insertTypeObject(
          acc.account,
          this.conn,
          Account,
        )

        const expense = this.repo.create({
          ...expData,
          account: accObj,
        })

        const addExpense = () => this.repo.save(expense)

        const updateAccount = () =>
          this.conn.getRepository(Account).update(
            { id: accObj.id },
            {
              balance: calcTransaction(accObj.balance, acc.amount, acc.process),
            },
          )

        return Promise.all([addExpense(), updateAccount()])
      })

      // ====================================================================================

      return Promise.all(accTransactions)
    }, options)
  }

  // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& Update Expense &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  // !! transfer account and loan is not up to date and going to be deprecated
  updateExpense = async (data: UpdateTransaction, options?: Options) => {
    return genPromise(async () => {
      // -------------------- Retrieve other DB data ---------------------------------
      const existingData: Expense = await insertTypeObject(
        data?.id,
        this.conn,
        Expense,
        ['account', 'loan', 'category'],
      )

      const category = await insertTypeObject(
        data?.category,
        this.conn,
        Category,
      )

      // Retrieve accounts
      const account: Account = await insertTypeObject(
        data?.account,
        this.conn,
        Account,
      )
      const transfer_account: Account = await insertTypeObject(
        data?.transfer_account,
        this.conn,
        Account,
      )

      // Retrieve Loans
      const loan: Loan = await insertTypeObject(data?.loan, this.conn, Loan)
      const transfer_loan: Loan = await insertTypeObject(
        data?.transfer_loan,
        this.conn,
        Loan,
      )

      // --------------------------------------------------------------------------------

      // Update if the account/loan
      const process = []
      const isTransactionChanged =
        data.transaction !== existingData?.transaction
      const isDiffAction = data.type !== existingData?.type
      const action = data.type as ExpenseProcess
      const reverseAction =
        data.type === ExpenseType.expense
          ? ExpenseType.income
          : ExpenseType.expense

      // ========================= Transfer account =========================================

      if (transfer_account?.id || existingData?.transfer_account?.id) {
        const isNewXferAccount =
          transfer_account?.id !== existingData?.transfer_account?.id ||
          isDiffAction

        if (isNewXferAccount) {
          const updateOldXferAccount = () =>
            this.conn.getRepository(Account).update(
              { id: existingData?.transfer_account?.id },
              {
                balance: calcTransaction(
                  existingData?.transfer_account?.balance,
                  data.transaction,
                  action,
                ),
              },
            )
          process.push(updateOldXferAccount)
        }

        if (isTransactionChanged || isNewXferAccount) {
          const balance = isNewXferAccount
            ? calcTransaction(
                transfer_account?.balance,
                data.transaction,
                action,
              )
            : updateExistingTransaction(
                transfer_account?.balance,
                data.transaction,
                existingData?.transaction,
                reverseAction,
              )

          const updateNewXferAccount = () =>
            this.conn.getRepository(Account).update(
              { id: transfer_account?.id },
              {
                balance,
              },
            )
          process.push(updateNewXferAccount)
        }
      }

      // ========================= Account =========================================

      if (account?.id || existingData?.account?.id) {
        const isAccountRemoved = !account?.id
        const hadAccount = existingData?.account?.id
        const isNewAccount =
          (!isAccountRemoved && account?.id !== existingData?.account?.id) ||
          isDiffAction

        if (hadAccount && (isNewAccount || isAccountRemoved)) {
          const updateOldAccount = () =>
            this.conn.getRepository(Account).update(
              { id: existingData?.account?.id },
              {
                balance: calcTransaction(
                  existingData?.account?.balance,
                  existingData.transaction,
                  isDiffAction ? action : reverseAction,
                ),
              },
            )
          process.push(updateOldAccount)
        }

        if ((isTransactionChanged || isNewAccount) && !isAccountRemoved) {
          const balance = isNewAccount
            ? calcTransaction(account?.balance, data?.transaction, action)
            : updateExistingTransaction(
                account?.balance,
                data?.transaction,
                existingData?.transaction,
                action,
              )

          const updateNewAccount = () => {
            this.conn.getRepository(Account).update(
              { id: account.id },
              {
                balance,
              },
            )
          }
          process.push(updateNewAccount)
        }
      }

      // ========================= Transfer loan =========================================

      if (transfer_loan?.id || existingData?.transfer_loan?.id) {
        const isNewXferLoan =
          transfer_loan.id !== existingData?.transfer_loan.id || isDiffAction

        if (isNewXferLoan) {
          const updateOldXferLoan = () =>
            this.conn.getRepository(Loan).update(
              { id: existingData?.transfer_loan.id },
              {
                balance: calcTransaction(
                  existingData?.transfer_loan.balance,
                  data.transaction,
                  action,
                ),
              },
            )

          process.push(updateOldXferLoan)
        }

        if (isTransactionChanged || isNewXferLoan) {
          const balance = isNewXferLoan
            ? calcTransaction(transfer_loan.balance, data.transaction, action)
            : updateExistingTransaction(
                transfer_loan.balance,
                data.transaction,
                existingData?.transaction,
                reverseAction,
              )

          const updateNewXferLoan = () =>
            this.conn
              .getRepository(Loan)
              .update({ id: transfer_loan.id }, { balance })

          process.push(updateNewXferLoan)
        }
      }

      // ========================= Loan =========================================

      if (loan?.id || existingData?.loan?.id) {
        const isLoanDeleted = !loan?.id
        const hadLoan = !!existingData?.loan?.id
        const isNewLoan =
          (!isLoanDeleted && loan?.id !== existingData?.loan?.id) ||
          isDiffAction

        // revert transaction in old loan if the loan account is different
        if (hadLoan && (isNewLoan || isLoanDeleted)) {
          const updateOldLoan = () =>
            this.conn.getRepository(Loan).update(
              { id: existingData?.loan.id },
              {
                balance: calcTransaction(
                  existingData?.loan.balance,
                  existingData.transaction,
                  isDiffAction ? action : reverseAction,
                ),
              },
            )

          process.push(updateOldLoan)
        }

        if (isTransactionChanged || isNewLoan) {
          const balance = isNewLoan
            ? calcTransaction(loan.balance, data.transaction, action)
            : updateExistingTransaction(
                loan.balance,
                data.transaction,
                existingData?.transaction,
                action,
              )

          const updateNewLoan = () =>
            this.conn.getRepository(Loan).update({ id: loan.id }, { balance })

          process.push(updateNewLoan)
        }
      }

      // ========================= Update Expense =========================================

      const updates: Partial<Expense> = {
        ...omit(data, ['account', 'transfer_account', 'loan', 'transfer_loan']),
        category,
        name: `${data?.name}`?.trim(),
        transaction: convertToInt(data.transaction),
      }

      if (account && data?.account) updates.account = account
      else updates.account = null

      if (transfer_account && data?.transfer_account)
        updates.transfer_account = transfer_account
      else updates.transfer_account = null

      if (loan && data?.loan) updates.loan = loan
      else updates.loan = null

      if (transfer_loan && data?.transfer_loan)
        updates.transfer_loan = transfer_loan
      else updates.transfer_loan = null

      const updateNewChanges = () => this.repo.update({ id: data.id }, updates)

      process.push(updateNewChanges)

      // ======================================================================

      return Promise.all(process.map(fn => fn && fn()))
    }, options)
  }

  // &&&&&&&&&&&&&&&&&&&&&&&&&& Delete Expense &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&

  deleteExpense = (target: string | Expense, options: Options) => {
    return genPromise(async () => {
      // -------------------- Retrieve other DB data ---------------------------------
      const existingData: Expense = await insertTypeObject(
        target,
        this.conn,
        Expense,
        ['account', 'loan', 'category'],
      )

      const process = []
      const reverseAction =
        existingData?.type === ExpenseType.expense
          ? ExpenseType.income
          : ExpenseType.expense

      // ========================= Update Account =========================================

      const accountId = existingData?.account?.id

      if (accountId) {
        const updateAccount = () =>
          this.conn.getRepository(Account).update(
            { id: accountId },
            {
              balance: calcTransaction(
                existingData?.account?.balance,
                existingData?.transaction,
                reverseAction,
              ),
            },
          )

        process.push(updateAccount)
      }

      // ========================= Update transfer Acccount ===================================

      const transferAccId = existingData?.transfer_account?.id

      if (transferAccId) {
        const updateXferAccount = () =>
          this.conn.getRepository(Account).update(
            { id: transferAccId },
            {
              balance: calcTransaction(
                existingData?.transfer_account?.balance,
                existingData?.transaction,
                existingData?.type as ExpenseProcess,
              ),
            },
          )

        process.push(updateXferAccount)
      }

      // ========================= Update loan =========================================

      const loanId = existingData?.loan?.id

      if (loanId) {
        const updateLoan = () =>
          this.conn.getRepository(Loan).update(
            { id: loanId },
            {
              balance: calcTransaction(
                existingData?.loan?.balance,
                existingData?.transaction,
                reverseAction,
              ),
            },
          )

        process.push(updateLoan)
      }

      // ========================= Update transfer loan =========================================

      const transferloanId = existingData?.transfer_loan?.id

      if (transferloanId) {
        const updateLoan = () =>
          this.conn.getRepository(Loan).update(
            { id: transferloanId },
            {
              balance: calcTransaction(
                existingData?.transfer_loan?.balance,
                existingData?.transaction,
                existingData?.type as ExpenseProcess,
              ),
            },
          )

        process.push(updateLoan)
      }

      // ========================= Delete existing expense =========================================

      const deleteExpense = () => this.repo.delete({ id: existingData?.id })

      process.push(deleteExpense)

      // ==================================================================================

      return Promise.all(process.map(p => p()))
    }, options)
  }
}
