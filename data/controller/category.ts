import { DataSource, Repository } from 'typeorm'
import { RepoData } from '..'
import { AuthObject } from '../../components/Authendication'
import { logError } from '../../utils/commonFunctions'
import {
  genPromise,
  insertTypeObject,
  Options,
} from '../../utils/functionCreater'
import { Category } from '../entities/category'
import { Expense } from '../entities/expense'
import { CategoryData } from '../interface/category'

export enum DeleteError {
  'EXP_EXIST' = 'EXP_EXIST',
  'ERR_CAUGHT' = 'ERR_CAUGHT',
}

export class CategoryController {
  private repo: Repository<Category>

  private conn: DataSource

  private userInfo: AuthObject

  constructor(args: RepoData) {
    this.repo = args.conn.getRepository(Category)
    this.conn = args.conn
    this.userInfo = args.authInfo
  }

  getAllCategories = (options?: Options): Promise<Category[]> => {
    return genPromise(
      () => this.repo.find({ where: { user_id: this.userInfo.uid } }),
      options,
    )
  }

  getCategory = (id: string, options?: Options): Promise<Category> => {
    return genPromise(() => this.repo.findOne({ where: { id } }), options)
  }

  addCategories = (data: CategoryData, options?: Options): Promise<any> => {
    const parentCategories = insertTypeObject(
      data.parent_category,
      this.conn,
      Category,
    )

    return genPromise(() => {
      const newCategory = this.repo.create({
        ...data,
        name: data?.name?.trim(),
        user_id: this.userInfo.uid,
        parent_category: parentCategories,
      })
      return this.repo.save(newCategory)
    }, options)
  }

  updateCategory = (
    id: string,
    data: Category,
    options?: Options,
  ): Promise<any> => {
    return genPromise(
      () =>
        this.repo.update(
          { id },
          {
            ...data,
            name: data?.name?.trim(),
          },
        ),
      options,
    )
  }

  removeCategory = async (data: Category, options?: Options): Promise<any> => {
    try {
      const expWithCat = await this.conn
        .getRepository(Expense)
        .findOne({ where: { category: data }, relations: ['category'] })

      if (!expWithCat?.id) {
        return genPromise(() => this.repo.delete({ id: data.id }), options)
      }

      return {
        internalError: DeleteError.EXP_EXIST,
      }
    } catch (e) {
      logError(e)

      return {
        internalError: DeleteError.ERR_CAUGHT,
      }
    }
  }
}
