import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('account')
export class Account {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  user_id!: string

  @Column({ unique: true })
  name!: string

  @Column()
  account_type!: string

  @Column({ nullable: true })
  balance!: number

  @Column({ nullable: true })
  min_balance!: number

  @Column({ nullable: true })
  exp_date!: Date

  @Column({ nullable: true })
  include_in_bal!: boolean

  @Column({ nullable: true })
  description!: string
}
