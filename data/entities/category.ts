import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'

@Entity('category')
export class Category {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  user_id!: string

  @Column()
  name!: string

  @ManyToOne(() => Category, { nullable: true })
  @JoinColumn()
  parent_category!: Category

  @Column({ nullable: true })
  description!: string
}
