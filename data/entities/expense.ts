import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm'
import { Account } from './account'
import { Category } from './category'
import { Loan } from './loan'

@Entity('expense')
export class Expense {
  @PrimaryGeneratedColumn('uuid')
  id!: string

  @Column()
  transaction_id!: string

  @Column()
  user_id!: string

  @Column()
  name!: string

  @Column()
  type!: string

  @ManyToOne(() => Category, { nullable: true })
  @JoinColumn()
  category!: Category

  @ManyToOne(() => Account, { nullable: true })
  @JoinColumn()
  account!: Account

  @ManyToOne(() => Account, { nullable: true })
  @JoinColumn()
  transfer_account!: Account

  @ManyToOne(() => Loan, { nullable: true })
  @JoinColumn()
  loan!: Loan

  @ManyToOne(() => Loan, { nullable: true })
  @JoinColumn()
  transfer_loan!: Loan

  @Column()
  transaction!: number

  @Column()
  date!: Date

  @Column({ nullable: true })
  reward_point!: number

  @Column({ nullable: true })
  exp_date!: Date

  @Column()
  doesnt_count!: boolean

  @Column({ nullable: true })
  description!: string
}
