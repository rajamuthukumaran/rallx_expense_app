import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('budget')
export class Budget {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column()
  user_id!: string

  @Column({ unique: true })
  name!: string

  @Column({ nullable: true })
  type!: string
}
