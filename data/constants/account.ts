export enum AccountTypes {
  'bank' = 'bank',
  'wallet' = 'wallet',
  'appCredit' = 'appCredit',
  'cash' = 'cash',
  'other' = 'other',
}

export const accountType = [
  {
    id: 'bank',
    name: 'Bank',
  },
  {
    id: 'wallet',
    name: 'Wallet',
  },
  {
    id: 'appCredit',
    name: 'App Credit',
  },
  {
    id: 'cash',
    name: 'Cash',
  },
  // {
  //   id: 'fd',
  //   name: 'Fixed deposit',
  // },
  // {
  //   id: 'mutualFund',
  //   name: 'Mutual Fund',
  // },
  // {
  //   id: 'investment',
  //   name: 'Investment',
  // },
  {
    id: 'other',
    name: 'Other',
  },
]

export const getAccountType = accountType.reduce((a, entry) => {
  return {
    ...a,
    [entry.id]: entry.name,
  }
}, {})
