import { NumChar } from '../../utils/commonTypes'
import { LoanTypes } from '../constants/loan'
import { Account } from '../entities/account'
import { Category } from '../entities/category'

export type LoanData = {
  name: string
  type: LoanTypes
  category?: string | Category
  account?: string | Account
  balance?: NumChar
  total_amount: NumChar
  payment?: NumChar
  opened_date?: Date
  exp_date?: Date
  due_date?: number
  interest?: NumChar
  fees?: NumChar
  default_reward_point?: NumChar
  round_off_reward?: boolean
  is_reloadable?: boolean
  is_autopay?: boolean
  description?: string
}

export type LoanFilter = {
  name?: string
  type?: LoanTypes | LoanTypes[]
  is_reloadable?: boolean
}
