export const palatte = {
  primary: {
    50: '#f9e4ff',
    100: '#e5b3ff',
    200: '#d282fd',
    300: '#c051fb',
    400: '#ad21f9',
    500: '#950bdf',
    600: '#7406ae',
    700: '#53037d',
    800: '#32014c',
    900: '#13001d',
  },
  secondary: {
    50: '#fee9fb',
    100: '#edc6e7',
    200: '#dda2d4',
    300: '#ce7dc2',
    400: '#bf59af',
    500: '#a64096',
    600: '#823175',
    700: '#5e2255',
    800: '#3a1333',
    900: '#180415',
  },
  white: {
    100: '#fff',
    200: '#fdfdfd',
    300: '#ddd',
    400: '#aaa',
  },
  black: {
    50: 'rgba(48, 48, 48, 0.75)',
    100: '#000',
    200: '#101010',
    300: '#333',
    400: '#363636',
  },
  blue: {
    50: '#e5f0ff',
    100: '#b9d2f9',
    200: '#8cb5f1',
    300: '#6097ec',
    400: '#3879e7',
    500: '#2260cd',
    600: '#194aa1',
    700: '#113573',
    800: '#062046',
    900: '#000b1b',
  },
  // red: {
  //   500: '#EB5757',
  // },
  // green: {
  //   500: '#1EFC1E',
  // },
}

export const colors = {
  purple100: palatte.primary[100],
  purple200: palatte.primary[200],
  purple300: palatte.primary[300],
  purple400: palatte.primary[400],
  main: palatte.primary[500],

  // positive: palatte.green[500],
  // negative: palatte.red[500],

  textW: palatte.white[100],
  textB: palatte.black[100],

  bgD: palatte.black[100],
  bgDL: palatte.black[50],
  bgL: palatte.white[200],
}
