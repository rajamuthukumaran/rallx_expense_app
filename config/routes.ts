export const routes = {
  login: 'login',
  home: 'home',

  dashboard: {
    default: 'dashboard',
  },

  expense: {
    default: 'expense',
    add: 'expense_add',
    edit: 'expense_edit',
  },

  account: {
    default: 'account',
    add: 'account_add',
  },

  loan: {
    default: 'loan',
    add: 'loan_add',
  },

  settings: {
    default: 'settings',
    categories: 'settings_category',
    data: 'settings_data',
    logs: 'settings_logs',
  },
}
