import { useGlobalValue } from '..'
import { useDatabase } from '../../data'
import { actions } from '../action'

const useBalanceAction = () => {
  const { state, dispatch } = useGlobalValue()
  const database = useDatabase()

  const updateBalance = (balance: number) =>
    dispatch({
      type: actions.UPDATE_BALANCE,
      payload: balance,
    })

  const reload = () => {
    database.accountRepo.getTotalAmount().then(res => updateBalance(res))
  }

  return {
    balance: state.balance.total,
    update: updateBalance,
    reload,
  }
}

export default useBalanceAction
