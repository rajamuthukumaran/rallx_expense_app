import { initState } from '.'
import { reducers } from './Reducers'

export type Action = {
  type: string
  payload: any
}

export const combineReducers =
  (reducer: { [x: string]: (arg0: any, arg1: any) => any }) =>
  (state: typeof initState, action: Action) =>
    Object.keys(reducer).reduce(
      (acc, prop) => ({
        ...acc,
        [prop]: reducer[prop](acc[prop], action),
      }),
      state,
    )

export const rootReducer = combineReducers(
  Object.keys(reducers).reduce(
    (acc, key) => ({
      ...acc,
      [key]: reducers[key].dispatch,
    }),
    {},
  ),
)
