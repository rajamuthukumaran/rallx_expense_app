import { actions } from '../action'
import { Action } from '../combinedReducer'

const initBalanaceState = {
  total: 0,
}

export const balance = {
  dispatch: (state: any, action: Action) => {
    const currentState: typeof initBalanaceState =
      state?.balance || initBalanaceState

    switch (action.type) {
      case actions.UPDATE_BALANCE: {
        return {
          ...currentState,
          total: action?.payload,
        }
      }

      default:
        return currentState
    }
  },

  state: initBalanaceState,
}
