import React, { useContext, useMemo, useReducer } from 'react'
import { ReactChild } from '../utils/commonTypes'
import { rootReducer } from './combinedReducer'
import { reducers } from './Reducers'
import { GlobalValueProps } from './type'

export const initState: GlobalValueProps = Object.keys(reducers).reduce(
  (acc, key) => ({
    ...acc,
    [key]: reducers[key].state,
  }),
  {},
) as GlobalValueProps

const GlobalProviderContext = React.createContext({
  state: initState,
  dispatch: undefined,
})

const GlobalValue: React.FC<ReactChild> = ({ children }) => {
  const [state, dispatch] = useReducer(rootReducer, initState)
  const store = useMemo(() => ({ state, dispatch }), [state])

  return (
    <GlobalProviderContext.Provider value={store}>
      {children}
    </GlobalProviderContext.Provider>
  )
}

export const useGlobalValue = () => {
  return useContext(GlobalProviderContext)
}

export default GlobalValue
