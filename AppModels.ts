import { routes } from './config/routes'
import AddAccount from './screens/Account/AddAccount'
import AddExpense from './screens/Expense/AddExpense'
import UpdateExpense from './screens/Expense/UpdateExpense'
import AddLoan from './screens/Loan/AddLoan'
import Category from './screens/Settings/Category'
import Data from './screens/Settings/Data'
import Logs from './screens/Settings/Logs'
import { RouteEntry } from './utils/commonTypes'

export const modelRouteMap: RouteEntry[] = [
  {
    name: routes.account.add,
    component: AddAccount,
  },

  {
    name: routes.expense.add,
    component: AddExpense,
  },
  {
    name: routes.expense.edit,
    component: UpdateExpense,
  },

  {
    name: routes.loan.add,
    component: AddLoan,
  },
  {
    name: routes.settings.categories,
    component: Category,
  },
  {
    name: routes.settings.data,
    component: Data,
  },
  {
    name: routes.settings.logs,
    component: Logs,
  },
]
