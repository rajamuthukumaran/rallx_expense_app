/* eslint-disable react/no-unstable-nested-components */
import React from 'react'
import { Box } from 'native-base'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import {
  MaterialIcons,
  Entypo,
  FontAwesome5,
  FontAwesome,
} from '@expo/vector-icons'

import { routes } from '../../config/routes'
import Expense from '../../screens/Expense'
import Account from '../../screens/Account'
import Settings from '../../screens/Settings'
import { colors } from '../../config/theme'
import { fonts, layout } from '../../mixins'
import AddExpense from '../../screens/Expense/AddExpense'
import Loan from '../../screens/Loan'

const Tab = createBottomTabNavigator()

const Navigation: React.FC = (): React.ReactElement => {
  return (
    <Box flex={1}>
      <Tab.Navigator
        initialRouteName={routes.expense.default}
        screenOptions={{
          headerShown: false,
          tabBarStyle: {
            height: 70,
            backgroundColor: '#420264aa',
            paddingBottom: 10,
            paddingTop: 10,
            paddingHorizontal: 10,
            borderTopColor: '#420264aa',
          },
          tabBarShowLabel: true,
          tabBarLabelStyle: {
            fontFamily: fonts.Montserrat500,
            fontSize: 11,
          },
          tabBarActiveTintColor: colors.purple200,
          tabBarInactiveTintColor: colors.textW,
        }}
      >
        {/* <Tab.Screen
          name={routes.dashboard.default}
          component={Dashboard}
          options={{
            tabBarLabel: 'Dashboard',
            tabBarIcon: ({ focused }) => (
              <MaterialIcons
                name="dashboard"
                size={24}
                color={focused ? colors.purple200 : colors.textW}
              />
            ),
          }}
        /> */}
        <Tab.Screen
          name={routes.expense.default}
          component={Expense}
          options={{
            tabBarIcon: ({ focused }) => (
              <FontAwesome5
                name="money-bill-wave"
                size={24}
                color={focused ? colors.purple200 : colors.textW}
              />
            ),
          }}
        />
        <Tab.Screen
          name={routes.account.default}
          component={Account}
          options={{
            tabBarIcon: ({ focused }) => (
              <Entypo
                name="wallet"
                size={24}
                color={focused ? colors.purple200 : colors.textW}
              />
            ),
          }}
        />
        <Tab.Screen
          name={`${routes.expense.add}_tab`}
          component={AddExpense}
          options={{
            tabBarLabel: '',
            tabBarIcon: () => (
              <Box
                marginBottom={7}
                bg="primary.200"
                borderRadius={50}
                padding={2}
                height={16}
                width={16}
                style={layout.center}
              >
                <MaterialIcons name="add" size={48} color={colors.textW} />
              </Box>
            ),
          }}
          listeners={({ navigation }) => ({
            tabPress: e => {
              e.preventDefault()
              navigation.navigate(routes.expense.add)
            },
          })}
        />
        <Tab.Screen
          name={routes.loan.default}
          component={Loan}
          options={{
            tabBarIcon: ({ focused }) => (
              <FontAwesome
                name="credit-card-alt"
                size={24}
                color={focused ? colors.purple200 : colors.textW}
              />
            ),
          }}
        />
        <Tab.Screen
          name={routes.settings.default}
          component={Settings}
          options={{
            tabBarIcon: ({ focused }) => (
              <MaterialIcons
                name="settings"
                size={24}
                color={focused ? colors.purple200 : colors.textW}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </Box>
  )
}

export default Navigation
