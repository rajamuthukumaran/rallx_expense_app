import React, { useEffect } from 'react'
import { Box } from 'native-base'
import Navigation from './Navigation'
import { ScreenContainer } from '../components/Layouts'
import { useAuth } from '../components/Authendication'
import { Text } from '../components/Input'
import { currency } from '../utils/commonFunctions'
import { fonts, layout } from '../mixins'
import useBalanceAction from '../GlobalValue/Actions/useBalanceAction'

const Home: React.FC = (): React.ReactElement => {
  const userInfo = useAuth()
  const { balance, reload } = useBalanceAction()

  // -----------------------------------------------------------------------

  useEffect(() => {
    reload()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <ScreenContainer withoutPadding>
      <Box style={layout.lrPartition} px={5} pt={2}>
        <Box style={layout.centerCol}>
          <Text fontSize="3xl" color="#6e318f" fontFamily={fonts.Montserrat500}>
            Rallx
          </Text>
        </Box>
        <Box>
          <Text textAlign="right">{userInfo.name || userInfo.email}</Text>
          <Text textAlign="right">{currency(balance)}</Text>
        </Box>
      </Box>
      <Navigation />
    </ScreenContainer>
  )
}

export default Home
