/* eslint-disable react/no-array-index-key */
import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { routes } from './config/routes'
import Home from './Home'
import { modelRouteMap } from './AppModels'
import { RouteEntry } from './utils/commonTypes'

const Stack = createNativeStackNavigator()

const AppNavigator: React.FC = (): React.ReactElement => {
  //! Temporarly disabled auth
  // const authInfo = useAuth()

  return (
    <Stack.Navigator
      initialRouteName={routes.login}
      screenOptions={{
        headerShown: false,
      }}
    >
      {/* {authInfo.isAuthendicated ? ( */}
      <>
        <Stack.Screen name={routes.home} component={Home} />
        <Stack.Group screenOptions={{ presentation: 'modal' }}>
          {modelRouteMap.map((model: RouteEntry, index) => (
            <Stack.Screen
              key={index}
              name={model.name}
              component={model.component}
            />
          ))}
        </Stack.Group>
      </>
      {/* ) : (
        <Stack.Screen
          name={routes.login}
          component={authInfo?.isCheckingAuth ? MoneyLoader : Login}
        />
      )} */}
    </Stack.Navigator>
  )
}

export default AppNavigator
