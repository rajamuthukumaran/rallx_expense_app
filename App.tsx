import 'reflect-metadata'
import 'intl'
import 'intl/locale-data/jsonp/en'
import 'react-native-get-random-values'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { StatusBar } from 'expo-status-bar'
import { SSRProvider } from '@react-aria/ssr'

import ThemeProvider from './components/ThemeProvider'
import { useMontserrat, usePoppins } from './hooks/fonts'
import SafeView from './components/SafeView'
import AppNavigator from './AppNavigator'
import Authendication from './components/Authendication'
import DataProvider from './data'
import GlobalValue from './GlobalValue'
import { MoneyLoader } from './components/FullscreenLoader'

const App: React.FC<any> = () => {
  // fonts
  const { isPoppinsloaded } = usePoppins()
  const { isMontserratLoaded } = useMontserrat()

  if (!isPoppinsloaded || !isMontserratLoaded) {
    return <MoneyLoader />
  }

  return (
    <SSRProvider>
      <ThemeProvider>
        {/* eslint-disable-next-line react/style-prop-object */}
        <StatusBar style="inverted" backgroundColor="#000" />
        <SafeView>
          <Authendication>
            <DataProvider>
              <GlobalValue>
                <NavigationContainer>
                  <AppNavigator />
                </NavigationContainer>
              </GlobalValue>
            </DataProvider>
          </Authendication>
        </SafeView>
      </ThemeProvider>
    </SSRProvider>
  )
}

export default App
