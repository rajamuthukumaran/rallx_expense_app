import {
  useFonts,
  Montserrat_100Thin,
  Montserrat_100Thin_Italic,
  Montserrat_200ExtraLight,
  Montserrat_200ExtraLight_Italic,
  Montserrat_300Light,
  Montserrat_300Light_Italic,
  Montserrat_400Regular,
  Montserrat_400Regular_Italic,
  Montserrat_500Medium,
  Montserrat_500Medium_Italic,
  Montserrat_600SemiBold,
  Montserrat_600SemiBold_Italic,
  Montserrat_700Bold,
  Montserrat_700Bold_Italic,
  Montserrat_800ExtraBold,
  Montserrat_800ExtraBold_Italic,
  Montserrat_900Black,
  Montserrat_900Black_Italic,
} from '@expo-google-fonts/montserrat'

const useMontserrat = (): { isMontserratLoaded: boolean } => {
  const [isMontserratLoaded] = useFonts({
    Montserrat100: Montserrat_100Thin,
    Montserrat100i: Montserrat_100Thin_Italic,
    Montserrat200: Montserrat_200ExtraLight,
    Montserrat200i: Montserrat_200ExtraLight_Italic,
    Montserrat300: Montserrat_300Light,
    Montserrat300i: Montserrat_300Light_Italic,
    Montserrat400: Montserrat_400Regular,
    Montserrat400i: Montserrat_400Regular_Italic,
    Montserrat500: Montserrat_500Medium,
    Montserrat500i: Montserrat_500Medium_Italic,
    Montserrat600: Montserrat_600SemiBold,
    Montserrat600i: Montserrat_600SemiBold_Italic,
    Montserrat700: Montserrat_700Bold,
    Montserrat700i: Montserrat_700Bold_Italic,
    Montserrat800: Montserrat_800ExtraBold,
    Montserrat800i: Montserrat_800ExtraBold_Italic,
    Montserrat900: Montserrat_900Black,
    Montserrat900i: Montserrat_900Black_Italic,
  })

  return { isMontserratLoaded }
}

export default useMontserrat
