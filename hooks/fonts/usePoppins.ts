import {
  useFonts,
  Poppins_100Thin,
  Poppins_100Thin_Italic,
  Poppins_200ExtraLight,
  Poppins_200ExtraLight_Italic,
  Poppins_300Light,
  Poppins_300Light_Italic,
  Poppins_400Regular,
  Poppins_400Regular_Italic,
  Poppins_500Medium,
  Poppins_500Medium_Italic,
  Poppins_600SemiBold,
  Poppins_600SemiBold_Italic,
  Poppins_700Bold,
  Poppins_700Bold_Italic,
  Poppins_800ExtraBold,
  Poppins_800ExtraBold_Italic,
  Poppins_900Black,
  Poppins_900Black_Italic,
} from '@expo-google-fonts/poppins'

const usePoppins = (): { isPoppinsloaded: boolean } => {
  const [isPoppinsloaded] = useFonts({
    Poppins100: Poppins_100Thin,
    Poppins100I: Poppins_100Thin_Italic,
    Poppins200: Poppins_200ExtraLight,
    Poppins200I: Poppins_200ExtraLight_Italic,
    Poppins300: Poppins_300Light,
    Poppins300I: Poppins_300Light_Italic,
    Poppins400: Poppins_400Regular,
    Poppins400I: Poppins_400Regular_Italic,
    Poppins500: Poppins_500Medium,
    Poppins500I: Poppins_500Medium_Italic,
    Poppins600: Poppins_600SemiBold,
    Poppins600I: Poppins_600SemiBold_Italic,
    Poppins700: Poppins_700Bold,
    Poppins700I: Poppins_700Bold_Italic,
    Poppins800: Poppins_800ExtraBold,
    Poppins800I: Poppins_800ExtraBold_Italic,
    Poppins900: Poppins_900Black,
    Poppins900I: Poppins_900Black_Italic,
  })

  return {
    isPoppinsloaded,
  }
}

export default usePoppins
