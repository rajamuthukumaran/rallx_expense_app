import { StyleSheet } from 'react-native'
import { colors } from '../config/theme'

export const fontsColor = StyleSheet.create({
  white: {
    color: colors.textW,
  },
  black: {
    color: colors.textB,
  },
  pve: {
    color: colors.positive,
  },
  neg: {
    color: colors.negative,
  },
  accent: {
    color: colors.main,
  },
})

export const fonts = {
  // Poppins
  Poppins100I: 'Poppins100I',
  Poppins200: 'Poppins200',
  Poppins200I: 'Poppins200I',
  Poppins300: 'Poppins300',
  Poppins300I: 'Poppins300I',
  Poppins400: 'Poppins400',
  Poppins400I: 'Poppins400I',
  Poppins500: 'Poppins500',
  Poppins500I: 'Poppins500I',
  Poppins600: 'Poppins600',
  Poppins600I: 'Poppins600I',
  Poppins700: 'Poppins700',
  Poppins700I: 'Poppins700I',
  Poppins800: 'Poppins800',
  Poppins800I: 'Poppins800I',
  Poppins900: 'Poppins900',
  Poppins900I: 'Poppins900I',

  // Montserrat
  Montserrat100: 'Montserrat100',
  Montserrat100I: 'Montserrat100I',
  Montserrat200: 'Montserrat200',
  Montserrat200I: 'Montserrat200I',
  Montserrat300: 'Montserrat300',
  Montserrat300I: 'Montserrat300I',
  Montserrat400: 'Montserrat400',
  Montserrat400I: 'Montserrat400I',
  Montserrat500: 'Montserrat500',
  Montserrat500I: 'Montserrat500I',
  Montserrat600: 'Montserrat600',
  Montserrat600I: 'Montserrat600I',
  Montserrat700: 'Montserrat700',
  Montserrat700I: 'Montserrat700I',
  Montserrat800: 'Montserrat800',
  Montserrat800I: 'Montserrat800I',
  Montserrat900: 'Montserrat900',
  Montserrat900I: 'Montserrat900I',
}
