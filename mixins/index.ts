import layout from './layout'
import { fontsColor, fonts } from './common'

export { layout, fontsColor, fonts }
