export type SignInFields = {
  email: string
  password: string
}

export type SignUpFields = SignInFields & {
  name: string
}
