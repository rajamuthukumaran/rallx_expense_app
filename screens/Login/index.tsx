import React, { useEffect, useState } from 'react'
import { Box, Button } from 'native-base'
import { useAuthRequest } from 'expo-auth-session/providers/google'
import { maybeCompleteAuthSession } from 'expo-web-browser'
import {
  getAuth,
  signInWithCredential,
  GoogleAuthProvider,
  onAuthStateChanged,
} from 'firebase/auth'
import { getDatabase, ref, set, update } from 'firebase/database'
import { NativeStackScreenProps } from '@react-navigation/native-stack'

import { AntDesign, Entypo } from '@expo/vector-icons'
import { TokenResponse } from 'expo-auth-session'
import { Text } from '../../components/Input'
// import env from '../../config/env'
import { logError } from '../../utils/commonFunctions'
import { useAuth } from '../../components/Authendication'
import EmailLogin from './EmailLogin'

maybeCompleteAuthSession()

const Login: React.FC<NativeStackScreenProps<any>> = ({
  navigation,
}): React.ReactElement => {
  const auth = getAuth()
  const database = getDatabase()
  const authData = useAuth()

  const [isEmailLogin, setIsEmailLogin] = useState(false)

  // --------------------------- Google SSO -----------------------------------
  const [request, response, promptAsync] = useAuthRequest({
    clientId: 'env.google.clientID', //! need to change after
    scopes: [
      'openid',
      'https://www.googleapis.com/auth/userinfo.profile',
      'https://www.googleapis.com/auth/userinfo.email',
    ],
  })

  const onSignIn = (googleUser: TokenResponse) => {
    const unsubscribe = onAuthStateChanged(auth, () => {
      unsubscribe()
      // Build Firebase credential with the Google ID token.
      const credential = GoogleAuthProvider.credential(
        googleUser.idToken,
        googleUser.accessToken,
      )

      // Sign in with credential from the Google user.
      signInWithCredential(auth, credential)
        .then(res => {
          // update firebase database
          const userId = res.user.email

          if (
            res.user.metadata.creationTime === res.user.metadata.lastSignInTime
          ) {
            const userRef = ref(database, `/users/${userId}`)

            set(userRef, {
              email: res.user.email,
              name: res.user.displayName,
              picture: res.user.photoURL,
              created_at: Date.now(),
              last_login: Date.now(),
              g_user_id: res.user.uid,
            })
              .then(() => {
                navigation.navigate('home')
              })
              .catch(err => logError(err))
          } else {
            const userRef = ref(database, `/users/${userId}`)
            update(userRef, {
              last_login: Date.now(),
            })
              .then(() => {
                navigation.navigate('home')
              })
              .catch(err => {
                logError(err)
              })
          }
        })
        .catch(error => {
          logError(error)
        })
    })
  }

  // monitor login status
  useEffect(() => {
    if (response?.type === 'success') {
      if (!response?.error) {
        onSignIn(response?.authentication)
      } else {
        logError(response.error)
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [response])

  //-------------------------------------------------------------------------

  return (
    <Box flex={1} px="12" py="24" bg="black.100" justifyContent="space-between">
      <Box>
        <Text fontSize="6xl" textAlign="center">
          RallX
        </Text>
        <Text fontSize="2xl" textAlign="center" color="primary.300">
          Expense
        </Text>
      </Box>
      {isEmailLogin ? (
        <EmailLogin goBack={() => setIsEmailLogin(false)} />
      ) : (
        <Box>
          <Button
            isDisabled={!request || authData?.isCheckingAuth}
            onPress={() => setIsEmailLogin(true)}
            startIcon={<Entypo name="mail" size={24} color="black" />}
            mb="4"
          >
            Sign in using email
          </Button>
          <Button
            isDisabled={!request || authData?.isCheckingAuth}
            onPress={() => promptAsync()}
            startIcon={<AntDesign name="google" size={24} color="black" />}
          >
            Sign in with Google
          </Button>
        </Box>
      )}
    </Box>
  )
}

export default Login
