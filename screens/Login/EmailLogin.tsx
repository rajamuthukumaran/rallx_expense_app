import React, { useState } from 'react'
import { Box } from 'native-base'
import { useForm } from 'react-hook-form'
import { Ionicons } from '@expo/vector-icons'
import {
  createUserWithEmailAndPassword,
  getAuth,
  signInWithEmailAndPassword,
} from 'firebase/auth'
import { getDatabase, ref, update, set } from 'firebase/database'
import { useNavigation } from '@react-navigation/native'
import { FormTextField } from '../../components/Form'
import { Button } from '../../components/Input/Button'
import { SignInFields, SignUpFields } from './type'
import { layout } from '../../mixins'
import { colors } from '../../config/theme'
import { logError } from '../../utils/commonFunctions'
import { routes } from '../../config/routes'

export type EmailLoginProps = { goBack: () => void }

const EmailLogin: React.FC<EmailLoginProps> = ({ goBack }) => {
  const auth = getAuth()
  const db = getDatabase()
  const navigation = useNavigation()
  const [isSignUp, setIsSignUp] = useState(false)

  // ------------------ React hook form initaliztation -------------------

  const {
    control,
    formState: { errors },
    handleSubmit,
  } = useForm<SignInFields | SignUpFields>({
    defaultValues: {
      email: '',
      password: '',
    },
  })

  const formProps = { control, errors }

  // -----------------------------------------------------------------------

  const onSubmit = async (data: SignUpFields) => {
    const { email, password } = data || {}
    if (password && email) {
      if (!isSignUp) {
        await signInWithEmailAndPassword(auth, email, password)
          .then(() => {
            const userId = email
            const userRef = ref(db, `/users/${userId}`)

            update(userRef, {
              last_login: Date.now(),
            })
              .then(() => {
                navigation.navigate(routes.home)
              })
              .catch(err => {
                logError(err)
              })
          })
          .catch(err => {
            logError(err)
          })
      } else if (data?.name) {
        await createUserWithEmailAndPassword(auth, email, password)
          .then(cred => {
            const userId = email
            const userRef = ref(db, `/users/${userId}`)

            set(userRef, {
              email: data?.email,
              name: data?.name,
              picture: cred.user.photoURL,
              created_at: Date.now(),
              last_login: Date.now(),
              fb_user_id: cred.user.uid,
            })
          })
          .then(() => {
            navigation.navigate(routes.home)
          })
          .catch(err => {
            logError(err)
          })
      }
    }
  }

  return (
    <Box style={layout.tdPartition} flex="1" py="4">
      <Box>
        {isSignUp && (
          <FormTextField
            label="Name"
            name="name"
            containerProps={{
              mb: 4,
            }}
            isRequired
            defaultValue=""
            {...formProps}
          />
        )}
        <FormTextField
          {...formProps}
          label="Email"
          name="email"
          isRequired
          containerProps={{
            mb: 4,
          }}
          rules={{
            pattern: {
              value:
                /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
              message: 'Please enter a valid email',
            },
          }}
        />
        <FormTextField
          label="Password"
          name="password"
          type="password"
          isRequired
          {...formProps}
        />

        <Button mt="8" onPress={handleSubmit(onSubmit)}>
          {isSignUp ? 'Sign up' : 'Sign in'}
        </Button>
        <Button
          mt="4"
          variant="ghost"
          onPress={() => setIsSignUp(prev => !prev)}
        >
          {isSignUp ? 'Sign in' : 'Sign up'}
        </Button>
      </Box>
      <Button
        mt="4"
        startIcon={
          <Ionicons name="chevron-back" size={24} color={colors.purple300} />
        }
        variant="ghost"
        onPress={goBack}
      >
        Back
      </Button>
    </Box>
  )
}

export default EmailLogin
