import React from 'react'
import Heading from '../../components/Heading'
import { ScreenContainer } from '../../components/Layouts'

const Dashboard: React.FC = (): React.ReactElement => {
  return (
    <ScreenContainer>
      <Heading label="Dashboard" />
    </ScreenContainer>
  )
}

export default Dashboard
