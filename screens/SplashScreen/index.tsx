import React from 'react'
import { View, Text as RNText } from 'react-native'
import SafeView from '../../components/SafeView'
import { colors } from '../../config/theme'

const SplashScreen: React.FC = () => {
  return (
    <SafeView>
      <View
        style={{
          backgroundColor: colors.bgD,
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <RNText
          style={{
            fontSize: 50,
            color: colors.textW,
          }}
        >
          Loading...
        </RNText>
      </View>
    </SafeView>
  )
}

export default SplashScreen
