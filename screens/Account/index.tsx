import React, { useState } from 'react'
import { Box, ScrollView } from 'native-base'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { isEmpty } from 'lodash'
import Heading from '../../components/Heading'
import { ScreenContainer } from '../../components/Layouts'
import { RoundButton } from '../../components/Input/Button'
import { fonts, layout } from '../../mixins'
import { routes } from '../../config/routes'
import { Account as IAccount } from '../../data/entities/account'
import { useDatabase } from '../../data'
import { getAccountType } from '../../data/constants/account'
import { Text } from '../../components/Input'
import AccountCard from './components/AccountCard'
import { LoadingText } from '../../components/Loader'
import useMountEffect from '../../hooks/useMountEffect'

export const accountScreens = {
  add: 'add',
}

const Account: React.FC<NativeStackScreenProps<any>> = ({ navigation }) => {
  const database = useDatabase()

  const [accountList, setAccountList] = useState<{ [key: string]: IAccount[] }>(
    {},
  )
  const [isLoading, setLoading] = useState(false)

  // -------------------- Database ops -------------------------------------

  const getAccounts = () => {
    database.accountRepo.getAllAccounts({ setLoading }).then(res => {
      const formattedRes = res.reduce((a, acc) => {
        const accType = acc.account_type
        return {
          ...a,
          [accType]: [...(a?.[accType] || []), acc],
        }
      }, {})
      setAccountList(formattedRes)
    })
  }

  // -----------------------------------------------------------------------

  const openAddAccount = (data?: IAccount) => {
    const path = routes.account.add
    navigation.push(path, {
      data,
    })
  }

  // ------------------------- Events -------------------------------------

  useMountEffect(() => {
    getAccounts()
  }, [])

  return (
    <ScreenContainer>
      <Box style={layout.lrPartition} pb="2">
        <Heading label="Accounts" />
        <RoundButton onPress={() => openAddAccount()} />
      </Box>

      {isLoading && isEmpty(accountList) ? (
        <LoadingText />
      ) : (
        <ScrollView>
          <Box>
            {Object.keys(accountList).map(accType => {
              return (
                <Box key={accType} mb="6">
                  <Text fontFamily={fonts.Montserrat600} color="primary.200">
                    {getAccountType?.[accType]}
                  </Text>
                  <Box>
                    {accountList?.[accType].map(acc => (
                      <AccountCard
                        key={acc.id}
                        data={acc}
                        onPress={openAddAccount}
                      />
                    ))}
                  </Box>
                </Box>
              )
            })}
          </Box>
        </ScrollView>
      )}
    </ScreenContainer>
  )
}

export default Account
