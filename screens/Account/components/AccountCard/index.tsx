import React from 'react'
import { Box, Pressable } from 'native-base'
import { get } from 'lodash'
import { Text } from '../../../../components/Input'
import { layout } from '../../../../mixins'
import { Account } from '../../../../data/entities/account'
import { currency } from '../../../../utils/commonFunctions'
import { getAccountType } from '../../../../data/constants/account'

export type AccountCardProps = {
  data: Account
  onPress: (data: Account) => void
}

const AccountCard: React.FC<AccountCardProps> = ({ data, onPress }) => {
  return (
    <Pressable onPress={() => onPress(data)}>
      <Box
        style={[layout.lrPartition]}
        bg="primary.800"
        borderRadius={15}
        p={5}
        my={2}
      >
        <Box style={[layout.asideCol]}>
          <Text>{data.name}</Text>
          <Text fontSize="xs" color="primary.100">
            {get(getAccountType, data.account_type)}
          </Text>
        </Box>

        <Box>
          <Text>{currency(data?.balance)}</Text>
        </Box>
      </Box>
    </Pressable>
  )
}

export default AccountCard
