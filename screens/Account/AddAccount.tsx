import React, { useMemo, useState } from 'react'
import { Box, Button } from 'native-base'
import { useForm } from 'react-hook-form'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { useDatabase } from '../../data'
import { AccountData } from '../../data/interface/account'
import { AccountTypes, accountType } from '../../data/constants/account'
import {
  FormSelectField,
  FormTextField,
  FormTextArea,
  FormDatePicker,
  FormCheckbox,
  FormNumberField,
} from '../../components/Form'
import { ModelContainer } from '../../components/Layouts'
import { nullify } from '../../utils/commonFunctions'
import useBalanceAction from '../../GlobalValue/Actions/useBalanceAction'
import { ConfirmationButton } from '../../components/Input/Button'

const AddAccount: React.FC<NativeStackScreenProps<any>> = ({
  navigation,
  route,
}): React.ReactElement => {
  const database = useDatabase()
  const { reload } = useBalanceAction()

  const [isLoading, setLoading] = useState(false)

  const existingData = useMemo(() => {
    return route.params?.data
  }, [route.params])

  // React hook form initaliztation ----------------------------
  const initData = {
    name: existingData?.name || '',
    account_type: existingData?.account_type || AccountTypes.bank,
    balance: `${existingData?.balance ?? ''}`,
    min_balance: `${existingData?.min_balance ?? ''}`,
    exp_date: existingData?.exp_date || '',
    description: existingData?.description || '',
    include_in_bal: existingData?.include_in_bal ?? true,
  }

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: initData,
  })

  const formProps = { control, errors, containerProps: { mb: 5 } }

  // -------------------- Database ops -------------------------------------

  const addAccount = (data: AccountData) => {
    database.accountRepo.addAccount(data).then(() => {
      reload()
      navigation.goBack()
    })
  }

  const updateAccount = (data: AccountData) => {
    database.accountRepo
      .updateAccount(existingData.id, data, { setLoading })
      .then(() => {
        reload()
        navigation.goBack()
      })
  }

  const deleteAccount = () => {
    if (existingData) {
      database.accountRepo
        .removeAccount(existingData?.id, { setLoading })
        .then(() => {
          reload()
          navigation.goBack()
        })
    }
  }

  // -----------------------------------------------------------------------

  const onSubmit = (data: AccountData) => {
    const filteredData = nullify(data)
    if (existingData) {
      updateAccount(filteredData)
    } else {
      addAccount(filteredData)
    }
  }

  return (
    <ModelContainer
      label={existingData ? 'Edit Account' : 'Add Account'}
      action={
        <Box>
          {!!existingData && (
            <ConfirmationButton
              bg="red.500"
              mb={5}
              label="Delete"
              disabled={isLoading}
              onApply={deleteAccount}
            />
          )}
          <Button disabled={isLoading} onPress={handleSubmit(onSubmit)}>
            Save
          </Button>
        </Box>
      }
    >
      <FormTextField name="name" label="Name" isRequired {...formProps} />

      <FormSelectField
        name="account_type"
        label="Account Type"
        isRequired
        options={accountType}
        {...formProps}
      />

      <FormNumberField
        name="balance"
        label="Balance"
        keyboardType="numeric"
        isCurrency
        {...formProps}
      />

      <FormNumberField
        name="min_balance"
        label="Minimum Balance"
        keyboardType="numeric"
        isCurrency
        {...formProps}
      />

      <FormDatePicker name="exp_date" label="Expiry Date" {...formProps} />

      <FormTextArea name="description" label="Description" {...formProps} />

      <FormCheckbox name="include_in_bal" {...formProps}>
        Include in balance
      </FormCheckbox>
    </ModelContainer>
  )
}

export default AddAccount
