import React from 'react'
import { AlertDialog, Box, Button } from 'native-base'
import { BackHandler } from 'react-native'

const RestartDialog: React.FC = () => {
  const cancelRef = React.useRef(null)

  const closeTheApp = () => {
    BackHandler.exitApp()
  }

  return (
    <AlertDialog leastDestructiveRef={cancelRef} isOpen onClose={closeTheApp}>
      <AlertDialog.Content>
        <AlertDialog.CloseButton />
        <AlertDialog.Header>Restored Successfully</AlertDialog.Header>
        <AlertDialog.Body>
          <Box>Please restart the app to continue</Box>
          <Box mt="5" alignItems="flex-end">
            <Button variant="ghost" onPress={closeTheApp}>
              Ok
            </Button>
          </Box>
        </AlertDialog.Body>
      </AlertDialog.Content>
    </AlertDialog>
  )
}

export default RestartDialog
