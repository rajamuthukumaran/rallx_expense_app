import React, { useState } from 'react'
import { Box, Button, Toast } from 'native-base'
import { MaterialIcons } from '@expo/vector-icons'
import * as Sharing from 'expo-sharing'
import * as FileSystem from 'expo-file-system'
import * as DocumentPicker from 'expo-document-picker'

import { ScreenModelContainer } from '../../../components/Layouts'
import { logError } from '../../../utils/commonFunctions'
import { Text } from '../../../components/Input'
import RestartDialog from './RestartDialog'

const Data: React.FC = () => {
  const [showPopup, setPopup] = useState(null)

  const dbPath = `${FileSystem.documentDirectory}SQLite`
  const dbURI = `${dbPath}/rallx_expense.db`

  const backupDB = () =>
    Sharing.shareAsync(dbURI, {
      dialogTitle: 'share or copy your DB via',
    }).catch(err => {
      logError(err)
    })

  const restoreDB = async (pathToDatabaseFile: string) => {
    const clonePath = pathToDatabaseFile
    const pathExt = clonePath.slice(-3)?.toLowerCase()
    if (pathExt === '.db') {
      if (!(await FileSystem.getInfoAsync(dbPath)).exists) {
        await FileSystem.makeDirectoryAsync(dbPath)
      }
      await FileSystem.copyAsync({
        from: pathToDatabaseFile,
        to: dbURI,
      })
        .then(() => setPopup('restore'))
        .catch(err => logError(err))
    } else {
      Toast.show({
        description:
          'Invalid backup file. Please select a valid .db file backed up from Rallx Expense',
      })
    }
  }

  const openDocPicker = () => {
    DocumentPicker.getDocumentAsync().then(res => {
      if (res?.type === 'success') {
        const uri = res?.uri
        restoreDB(uri)
      }
    })
  }

  return (
    <ScreenModelContainer label="Manage data">
      <Box mt="4">
        <Text fontSize="3xl" fontWeight="bold" color="primary.500" mb={4}>
          Backup
        </Text>
        <Text mb={4}>
          You can backup your data as a .db file to your storage or send it
          through email, Bluetooth, telegram, etc. You will also be able to
          modify your data from a database manager like DBeaver.
        </Text>
        <Button
          onPress={backupDB}
          startIcon={<MaterialIcons name="backup" size={24} color="white" />}
        >
          Backup
        </Button>
      </Box>

      <Box mt="12">
        <Text fontSize="3xl" fontWeight="bold" color="primary.500" mb={4}>
          Restore
        </Text>
        <Text mb={4}>
          You can restore your data from .db file backed up from Rallx Expense
        </Text>
        <Button
          onPress={openDocPicker}
          startIcon={<MaterialIcons name="restore" size={24} color="white" />}
        >
          Restore
        </Button>
      </Box>

      {showPopup === 'restore' && <RestartDialog />}
    </ScreenModelContainer>
  )
}

export default Data
