import React from 'react'
import { Box, Pressable, Text } from 'native-base'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { Entypo, MaterialIcons } from '@expo/vector-icons'
import Heading from '../../components/Heading'
import { ScreenContainer } from '../../components/Layouts'
import { IconType, settingsOptions } from './constant'
import { layout } from '../../mixins'
import { colors } from '../../config/theme'

const Settings: React.FC<NativeStackScreenProps<any>> = ({
  navigation,
}): React.ReactElement => {
  // const handleSignOut = () => {
  //   const auth = getAuth()
  //   signOut(auth)
  // }

  const openSetting = (path: string) => {
    navigation.push(path)
  }

  return (
    <ScreenContainer hasPartiton>
      <Box>
        <Heading>Settings</Heading>
        <Box mt="8">
          {settingsOptions.map(opt => (
            <Pressable
              mb={5}
              key={opt.name}
              onPress={() => openSetting(opt.path)}
            >
              <Box style={layout.asideRow}>
                {opt?.iconType === IconType.Entypo ? (
                  <Entypo
                    name={opt.icon as any}
                    size={32}
                    color={colors.purple200}
                    style={{ marginRight: 14 }}
                  />
                ) : (
                  <MaterialIcons
                    name={opt.icon as any}
                    size={32}
                    color={colors.purple200}
                    style={{ marginRight: 14 }}
                  />
                )}
                <Text fontSize="xl" mt={1}>
                  {opt.name}
                </Text>
              </Box>
            </Pressable>
          ))}
        </Box>
      </Box>

      <Box mb={10}>
        {/* <Button h={45} onPress={handleSignOut}>
          Sign out
        </Button> */}
      </Box>
    </ScreenContainer>
  )
}

export default Settings
