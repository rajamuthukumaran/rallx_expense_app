import { routes } from '../../config/routes'

export enum IconType {
  MaterialIcons,
  Entypo,
}

export const settingsOptions = [
  {
    name: 'Category',
    path: routes.settings.categories,
    icon: 'category',
  },
  {
    name: 'Data',
    path: routes.settings.data,
    icon: 'database',
    iconType: IconType.Entypo,
  },
  {
    name: 'Error Logs',
    path: routes.settings.logs,
    icon: 'bug',
    iconType: IconType.Entypo,
  },
]
