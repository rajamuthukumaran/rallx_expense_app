import React, { useState } from 'react'
import { Box, IconButton } from 'native-base'
import { MaterialIcons } from '@expo/vector-icons'
import { Category } from '../../../data/entities/category'
import { layout } from '../../../mixins'
import { Text, TextField } from '../../../components/Input'
import { ConfirmationButton } from '../../../components/Input/Button'

export type EntryProps = {
  data: Category
  isLoading?: boolean
  onUpdate: (data: Category) => void
  onDelete: (data: Category) => void
}

const Entry: React.FC<EntryProps> = ({
  data,
  onDelete,
  onUpdate,
  isLoading,
}) => {
  const [isEdit, setEdit] = useState(false)
  const [newName, setNewName] = useState(data?.name || '')

  const handleUpdate = () => {
    onUpdate({
      ...data,
      name: newName,
    })
    setEdit(false)
  }

  return (
    <Box
      bg="primary.800"
      my="2"
      p="4"
      borderRadius="15"
      style={layout.lrPartition}
    >
      <Box>
        {isEdit ? (
          <TextField
            value={newName}
            onChangeText={setNewName}
            variant="underlined"
            fontSize="md"
          />
        ) : (
          <Text onPress={() => setEdit(true)}>{data.name}</Text>
        )}
      </Box>
      <Box style={layout.asideRow}>
        {isEdit && (
          <ConfirmationButton
            disabled={isLoading}
            onApply={handleUpdate}
            onCancel={() => setEdit(false)}
            render={(onPress, trigger) => (
              <IconButton
                {...trigger}
                variant="ghost"
                _icon={{
                  as: MaterialIcons,
                  name: 'check',
                  color: 'white.100',
                  size: 6,
                }}
                borderRadius={50}
                onPress={onPress}
              />
            )}
          />
        )}

        <ConfirmationButton
          disabled={isLoading}
          onApply={() => onDelete(data)}
          onCancel={() => setEdit(false)}
          render={(onPress, trigger) => (
            <IconButton
              {...trigger}
              variant="ghost"
              _icon={{
                as: MaterialIcons,
                name: 'delete',
                color: 'white.100',
                size: 6,
              }}
              borderRadius={50}
              onPress={onPress}
              ml="2"
            />
          )}
        />
      </Box>
    </Box>
  )
}

export default Entry
