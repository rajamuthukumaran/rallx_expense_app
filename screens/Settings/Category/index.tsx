import React, { useState } from 'react'
import { Box, IconButton, useToast } from 'native-base'
import { MaterialIcons } from '@expo/vector-icons'

import { ScreenModelContainer } from '../../../components/Layouts'
import { useDatabase } from '../../../data'
import { Category as ICategory } from '../../../data/entities/category'
import useMountEffect from '../../../hooks/useMountEffect'
import { TextField } from '../../../components/Input'
import { layout } from '../../../mixins'
import Entry from './Entry'
import { DeleteError } from '../../../data/controller/category'

const Category: React.FC = () => {
  const database = useDatabase()
  const toast = useToast()

  const [categories, setCategories] = useState<ICategory[]>([])
  const [isLoading, setLoading] = useState(false)

  const [newCategory, setNewCategory] = useState('')

  // -------------------- Database ops -------------------------------------

  const getCategories = () => {
    database.categoryRepo
      .getAllCategories({ setLoading })
      .then(res => setCategories(res))
  }

  const addCategory = () => {
    if (newCategory) {
      if (!categories?.find(f => f.name === newCategory)) {
        const req = {
          name: newCategory,
        }

        database.categoryRepo.addCategories(req, { setLoading }).then(() => {
          getCategories()
          setNewCategory('')
        })
      } else {
        toast.show({
          description: 'Category already exists',
          bgColor: 'red.400',
          color: 'error.50',
        })
      }
    }
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const updateCategory = (data: ICategory) => {
    database.categoryRepo.addCategories(data, { setLoading }).then(() => {
      getCategories()
    })
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const deleteCategory = (data: ICategory) => {
    database.categoryRepo.removeCategory(data, { setLoading }).then(res => {
      getCategories()

      if (res?.internalError || res?.internalError === 0) {
        const errCode = res?.internalError

        if (errCode === DeleteError.EXP_EXIST) {
          toast.show({
            description:
              'This category is already in use, so this cannot be deleted',
            bgColor: 'red.400',
            color: 'error.50',
          })
        }
      }
    })
  }

  // ------------------------- Events -------------------------------------

  useMountEffect(() => {
    getCategories()
  }, [])

  // -----------------------------------------------------------------------

  return (
    <ScreenModelContainer
      label="Category"
      action={
        <Box px={4} py={4} m="-3" bg="primary.800" style={layout.lrPartition}>
          <Box flex={1} mr={3}>
            <TextField
              bg="black.100"
              placeholder="Enter the category..."
              value={newCategory}
              onChangeText={value => setNewCategory(value)}
              isDisabled={isLoading}
            />
          </Box>
          <Box mr={-2}>
            <IconButton
              variant="solid"
              _icon={{
                as: MaterialIcons,
                name: 'add',
                color: 'white.100',
                size: 8,
              }}
              borderRadius={1000}
              onPress={() => addCategory()}
              isDisabled={isLoading}
            />
          </Box>
        </Box>
      }
    >
      <Box>
        {categories?.map(cat => {
          return (
            <Box key={cat.id}>
              <Entry
                data={cat}
                isLoading={isLoading}
                onUpdate={updateCategory}
                onDelete={deleteCategory}
              />
            </Box>
          )
        })}
      </Box>
    </ScreenModelContainer>
  )
}

export default Category
