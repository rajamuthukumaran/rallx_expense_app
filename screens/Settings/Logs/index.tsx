import { Box, Button } from 'native-base'
import React, { useEffect, useState } from 'react'
import { Text } from '../../../components/Input'
import Switch from '../../../components/Input/Switch'
import { ScreenModelContainer } from '../../../components/Layouts'
import { layout } from '../../../mixins'
import {
  getAsyncData,
  removeAsyncData,
  setAsyncData,
} from '../../../utils/asyncStorage'

const Logs: React.FC = () => {
  const [isLogOn, setLogOn] = useState(false)
  const [logs, setLogs] = useState([])

  const getLogstatus = async () => {
    const logStatus = await getAsyncData('shouldLogErrors')
    setLogOn(!!logStatus)
  }

  const toggleLogStatus = () => {
    setAsyncData('shouldLogErrors', !isLogOn)
    setLogOn(prev => !prev)
  }

  const getLogs = async () => {
    const logsData: string[] = await getAsyncData('errorLogs')
    setLogs(logsData || [])
  }

  const clearLogs = () => {
    removeAsyncData('errorLogs')
    getLogs()
  }

  useEffect(() => {
    getLogstatus()
  }, [])

  useEffect(() => {
    if (isLogOn) {
      getLogs()
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLogOn])

  return (
    <ScreenModelContainer
      label="Error Log"
      titleCompanion={<Switch isChecked={isLogOn} onChange={toggleLogStatus} />}
      action={
        <Box style={layout.lrPartition} w="100%" px="2" py="6">
          <Button
            w="135"
            bg="black.300"
            borderRadius="full"
            onPress={clearLogs}
          >
            Clear
          </Button>
          <Button w="135" borderRadius="full" onPress={getLogs}>
            Reload
          </Button>
        </Box>
      }
    >
      <Box>
        {logs?.map((log, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <Box p="4" my="2" bg="black.300" borderRadius="2xl" key={index}>
            <Text>{log}</Text>
          </Box>
        ))}
      </Box>
    </ScreenModelContainer>
  )
}

export default Logs
