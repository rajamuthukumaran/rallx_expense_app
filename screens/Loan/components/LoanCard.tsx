import React from 'react'
import { Box, Pressable, Text } from 'native-base'
import { Loan } from '../../../data/entities/loan'
import { fonts, layout } from '../../../mixins'
import { currency, dateFmt } from '../../../utils/commonFunctions'

export type LoanCardProps = {
  data: Loan
  onPress: (data: Loan) => void
}

const LoanCard: React.FC<LoanCardProps> = ({ data, onPress }) => {
  const hasDate = data?.opened_date || data?.exp_date
  const accountName = data?.account?.name

  return (
    <Pressable onPress={() => onPress(data)}>
      <Box bg="primary.800" borderRadius={15} p={5} my={2}>
        {!!accountName && (
          <Box mb={2}>
            <Text color="primary.200" fontFamily={fonts.Montserrat700}>
              {accountName}
            </Text>
          </Box>
        )}

        <Box style={layout.lrPartition}>
          <Box style={layout.asideCol}>
            <Text>{data?.name}</Text>
            <Text fontSize="xs" color="primary.100">
              {currency(data?.total_amount)}
            </Text>
          </Box>
          <Text color={data?.total_amount > 0 ? 'red.500' : 'green.500'}>
            {currency(data.balance)}
          </Text>
        </Box>

        {hasDate && (
          <Box mt={4} style={layout.lrPartition}>
            <Text fontSize="xs" color="primary.100">
              {dateFmt(data?.opened_date) || 'NA'}
            </Text>
            <Box
              flex="1"
              ml={4}
              borderBottomColor="primary.100"
              borderBottomWidth={1}
            />
            <Box
              borderBottomWidth={6}
              borderTopWidth={6}
              borderLeftWidth={6}
              borderLeftColor="primary.200"
              mr={4}
            />
            <Text fontSize="xs" color="primary.100">
              {dateFmt(data.exp_date) || 'NA'}
            </Text>
          </Box>
        )}
      </Box>
    </Pressable>
  )
}

export default LoanCard
