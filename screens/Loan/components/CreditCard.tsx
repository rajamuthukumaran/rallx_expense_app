import React, { useMemo } from 'react'
import { Box, Center, Pressable, Text } from 'native-base'
import { Loan } from '../../../data/entities/loan'
import { layout } from '../../../mixins'
import { currency } from '../../../utils/commonFunctions'

export type CreditCardProps = {
  data: Loan
  onPress: (data: Loan) => void
}

const CreditCard: React.FC<CreditCardProps> = ({ data, onPress }) => {
  const payment = useMemo(() => {
    return (data?.total_amount || 0) - (data?.balance || 0)
  }, [data?.total_amount, data?.balance])

  return (
    <Pressable onPress={() => onPress(data)}>
      <Box bg="primary.800" borderRadius={15} p={5} my={2}>
        <Box>
          <Box style={layout.lrPartition}>
            <Box style={layout.asideCol}>
              <Text fontSize="xl">{data?.name}</Text>
              <Text fontSize="xs" color="primary.100">
                Credit Card
              </Text>
            </Box>
          </Box>
        </Box>

        <Box mt="4">
          <Box style={layout.lrPartition}>
            <Text>Balance</Text>
            <Box style={layout.asideRow}>
              <Text
                color={data?.balance > 0 ? 'green.500' : 'red.500'}
                fontSize="md"
              >{`${currency(data?.balance)}`}</Text>
              <Text color="primary.200" fontSize="md">{` / ${currency(
                data?.total_amount,
              )}`}</Text>
            </Box>
          </Box>
          <Box style={layout.lrPartition}>
            <Text>Payment</Text>
            <Text color={payment > 0 ? 'red.500' : 'green.500'}>
              {currency(payment)}
            </Text>
          </Box>
        </Box>

        {!!data?.due_date && (
          <Center>
            <Text color="primary.200" fontSize="xs" mt={4}>
              Due on {data?.due_date} of every month
            </Text>
          </Center>
        )}
      </Box>
    </Pressable>
  )
}

export default CreditCard
