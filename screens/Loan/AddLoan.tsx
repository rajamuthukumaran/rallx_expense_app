import React, { useEffect, useMemo, useState } from 'react'
import { Box, Button } from 'native-base'
import { useForm } from 'react-hook-form'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { useDatabase } from '../../data'
import {
  FormSelectField,
  FormTextField,
  FormTextArea,
  FormDatePicker,
  FormCheckbox,
  FormNumberField,
} from '../../components/Form'
import { ModelContainer } from '../../components/Layouts'
import { nullify } from '../../utils/commonFunctions'
import { loanType, LoanTypes } from '../../data/constants/loan'
import { LoanData } from '../../data/interface/loan'
import { Loan } from '../../data/entities/loan'
import { ConfirmationButton, CustomButton } from '../../components/Input/Button'
import useBalanceAction from '../../GlobalValue/Actions/useBalanceAction'
import { Text } from '../../components/Input'

const AddLoan: React.FC<NativeStackScreenProps<any>> = ({
  navigation,
  route,
}): React.ReactElement => {
  const database = useDatabase()
  const { reload } = useBalanceAction()

  const [accountList, setAccountList] = useState<
    { name: string; id: string }[]
  >([])
  const [isAccountLoading, setAccountLoading] = useState(false)
  const [isLoading, setLoading] = useState(false)

  const existingData: Loan = useMemo(() => {
    return route.params?.data
  }, [route.params])

  // React hook form initaliztation ----------------------------
  const initData: LoanData = {
    name: existingData?.name || '',
    type: (existingData?.type as LoanTypes) || LoanTypes.EMI,
    balance: `${existingData?.balance ?? ''}`,
    total_amount: `${existingData?.total_amount ?? ''}`,
    interest: `${existingData?.interest ?? ''}`,
    fees: `${existingData?.fees ?? ''}`,
    account: `${existingData?.account ?? ''}`,
    category: `${existingData?.category ?? ''}`,
    payment: `${existingData?.payment ?? ''}`,
    default_reward_point: `${existingData?.default_reward_point ?? ''}`,
    description: existingData?.description || '',
    is_reloadable: existingData?.is_reloadable ?? true,
    is_autopay: existingData?.is_autopay ?? false,
    round_off_reward: existingData?.round_off_reward ?? true,
    due_date: existingData?.due_date ?? undefined,
    opened_date: existingData?.opened_date ?? undefined,
    exp_date: existingData?.exp_date ?? undefined,
  }

  const {
    control,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({
    defaultValues: initData,
  })

  const formProps = { control, errors, containerProps: { mb: 5 } }

  // -------------------- Database ops -------------------------------------

  const getAccounts = () => {
    database.accountRepo
      .getAllAccounts({ setLoading: setAccountLoading })
      .then(res =>
        setAccountList(
          res.map(acc => ({
            name: acc.name,
            id: acc.id,
          })),
        ),
      )
  }

  const addLoan = (data: LoanData) => {
    database.loanRepo.addLoan(data).then(() => {
      reload()
      navigation.goBack()
    })
  }

  const updateLoan = (data: LoanData) => {
    database.loanRepo
      .updateLoan(existingData?.id, data, { setLoading })
      .then(() => {
        reload()
        navigation.goBack()
      })
  }

  const deleteAccount = () => {
    if (existingData) {
      database.loanRepo
        .removeLoan(existingData?.id, { setLoading })
        .then(() => {
          reload()
          navigation.goBack()
        })
    }
  }

  // ------------------------- Events -------------------------------------

  useEffect(() => {
    getAccounts()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  // -----------------------------------------------------------------------

  const onSubmit = (data: LoanData) => {
    const filteredData = nullify(data)
    if (existingData) {
      updateLoan(filteredData)
    } else {
      addLoan(filteredData)
    }
  }

  const errMsg = {
    dueDate: 'Invalid due date, the date should be between 1 and 28',
  }

  return (
    <ModelContainer
      label={`${existingData ? 'Edit' : 'Add'} Loan`}
      action={
        <Box>
          {!!existingData && (
            <ConfirmationButton
              bg="red.500"
              mb={5}
              label="Delete"
              disabled={isLoading}
              onApply={deleteAccount}
            />
          )}
          <Button disabled={isLoading} onPress={handleSubmit(onSubmit)}>
            Save
          </Button>
        </Box>
      }
    >
      <FormTextField name="name" label="Name" isRequired {...formProps} />

      <FormSelectField
        name="type"
        label="loan Type"
        isRequired
        options={loanType}
        {...formProps}
      />
      <FormSelectField
        name="account"
        label="Account"
        isDisabled={isLoading || isAccountLoading}
        options={accountList}
        {...formProps}
      />
      <CustomButton
        mt="-2"
        mb="2"
        alignItems="flex-end"
        onPress={() => setValue('account', '')}
      >
        <Text color="primary.200">Clear</Text>
      </CustomButton>

      <FormNumberField
        name="balance"
        label="Balance"
        keyboardType="numeric"
        isCurrency
        {...formProps}
      />
      <FormNumberField
        name="total_amount"
        label="Total Amount"
        keyboardType="numeric"
        isCurrency
        isRequired
        {...formProps}
      />

      <FormNumberField
        name="payment"
        label="Payment"
        keyboardType="numeric"
        isCurrency
        {...formProps}
      />

      <FormNumberField
        name="interest"
        label="Interest"
        keyboardType="numeric"
        {...formProps}
      />
      <FormNumberField
        name="fees"
        label="Fees"
        keyboardType="numeric"
        isCurrency
        {...formProps}
      />
      <FormTextField
        name="due_date"
        label="Due date"
        keyboardType="numeric"
        rules={{
          min: {
            value: 1,
            message: errMsg.dueDate,
          },
          max: {
            value: 28,
            message: errMsg.dueDate,
          },
        }}
        {...formProps}
      />

      <FormDatePicker name="opened_date" label="Opened Date" {...formProps} />
      <FormDatePicker name="exp_date" label="Expiry Date" {...formProps} />

      <FormTextArea name="description" label="Description" {...formProps} />

      <FormCheckbox mb="2" name="is_reloadable" {...formProps}>
        Is revolving account
      </FormCheckbox>
      <FormCheckbox name="is_autopay" {...formProps}>
        Has Autopay
      </FormCheckbox>
    </ModelContainer>
  )
}

export default AddLoan
