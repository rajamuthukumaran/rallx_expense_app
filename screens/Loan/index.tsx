import React, { useState } from 'react'
import { Box, ScrollView } from 'native-base'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { isEmpty, omit } from 'lodash'
import Heading from '../../components/Heading'
import { ScreenContainer } from '../../components/Layouts'
import { RoundButton } from '../../components/Input/Button'
import { fonts, layout } from '../../mixins'
import { routes } from '../../config/routes'
import { Loan as ILoan } from '../../data/entities/Loan'
import { useDatabase } from '../../data'
import { Text } from '../../components/Input'
import { LoadingText } from '../../components/Loader'
import useMountEffect from '../../hooks/useMountEffect'
import CreditCard from './components/CreditCard'
import LoanCard from './components/LoanCard'
import { getLoanType } from '../../data/constants/loan'

const Loan: React.FC<NativeStackScreenProps<any>> = ({ navigation }) => {
  const database = useDatabase()

  const [loanList, setLoanList] = useState<{ [key: string]: ILoan[] }>({})
  const [isLoading, setLoading] = useState(false)

  // -------------------- Database ops -------------------------------------

  const getLoans = () => {
    database.loanRepo.getAllLoans({ setLoading }).then(res => {
      const formattedRes = res.reduce((a, loan) => {
        const loanType = loan.type
        return {
          ...a,
          [loanType]: [...(a?.[loanType] || []), loan],
        }
      }, {})
      setLoanList(formattedRes)
    })
  }

  // -----------------------------------------------------------------------

  const openAddLoan = (data?: ILoan) => {
    const path = routes.loan.add
    navigation.push(path, {
      data,
    })
  }

  // ------------------------- Events -------------------------------------

  useMountEffect(() => {
    getLoans()
  }, [])

  return (
    <ScreenContainer>
      <Box style={layout.lrPartition} pb="2">
        <Heading label="Loans" />
        <RoundButton onPress={() => openAddLoan()} />
      </Box>

      {isLoading && isEmpty(loanList) ? (
        <LoadingText />
      ) : (
        <ScrollView>
          <Box>
            {loanList?.credit_card?.length && (
              <Box mb="6">
                <Text fontFamily={fonts.Montserrat600} color="primary.200">
                  Credit Cards
                </Text>
                <Box>
                  {loanList?.credit_card?.map(cc => (
                    <CreditCard
                      key={cc.id}
                      data={cc}
                      onPress={data => openAddLoan(data)}
                    />
                  ))}
                </Box>
              </Box>
            )}
            {Object.keys(omit(loanList, 'credit_card')).map(loanKeys => {
              return (
                <Box key={loanKeys} mb="6">
                  <Text fontFamily={fonts.Montserrat600} color="primary.200">
                    {getLoanType?.[loanKeys]}
                  </Text>
                  <Box>
                    {loanList?.[loanKeys].map(loan => (
                      <LoanCard
                        key={loan.id}
                        data={loan}
                        onPress={data => openAddLoan(data)}
                      />
                    ))}
                  </Box>
                </Box>
              )
            })}
          </Box>
        </ScrollView>
      )}
    </ScreenContainer>
  )
}

export default Loan
