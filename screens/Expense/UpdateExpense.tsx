import React, { useMemo, useState } from 'react'
import { Box, Button, Switch } from 'native-base'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { useForm } from 'react-hook-form'
import { ModelContainer } from '../../components/Layouts'
import { ConfirmationButton } from '../../components/Input/Button'
import { Account } from '../../data/entities/account'
import { useDatabase } from '../../data'
import { Expense } from '../../data/entities/expense'
import { UpdateTransaction } from '../../data/interface/expense'
import {
  FormDatePicker,
  FormNumberField,
  FormRadioGroup,
  FormSelectField,
  FormTextArea,
  FormTextField,
} from '../../components/Form'
import { processOptions } from './component/AccountSelector'
import useMountEffect from '../../hooks/useMountEffect'
import { Loan } from '../../data/entities/loan'
import { layout } from '../../mixins'
import useBalanceAction from '../../GlobalValue/Actions/useBalanceAction'

const UpdateExpense: React.FC<NativeStackScreenProps<any>> = ({
  navigation,
  route,
}) => {
  const database = useDatabase()
  const { reload } = useBalanceAction()
  const existingData: Expense = useMemo(() => {
    return route.params?.data
  }, [route.params])

  const [accountList, setAccountList] = useState<Account[]>([])
  const [loanList, setLoanList] = useState<Loan[]>([])
  const [isLoan, setLoan] = useState(!!existingData?.loan)

  // Loaders
  const [isLoading, setLoading] = useState(false)
  const [isAccountLoading, setAccountLoading] = useState(false)
  const [isLoanLoading, setLoanLoading] = useState(false)

  // ------------------ React hook form initaliztation -------------------

  const initData: UpdateTransaction = {
    ...existingData,
    name: existingData?.name || '',
    account: existingData?.account?.id || '',
    loan: existingData?.loan?.id || '',
    transfer_account: existingData?.transfer_account?.id || '',
    transfer_loan: existingData?.transfer_loan?.id || '',
    date: existingData?.date || new Date(),
    description: existingData?.description || '',
    transaction: `${existingData?.transaction || ''}`,
  }

  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm<any>({
    defaultValues: initData,
  })

  const formProps = {
    control,
    errors,
    containerProps: { mb: 5 },
    isDisabled: isLoading,
  }

  // -------------------- Database ops -------------------------------------

  const getAccounts = () => {
    database.accountRepo
      .getAllAccounts({ setLoading: setAccountLoading })
      .then(res => setAccountList(res))
  }

  const getLoan = () => {
    database.loanRepo
      .getAllLoans({
        setLoading: setLoanLoading,
        canUseToPay: true,
      })
      .then(res => setLoanList(res))
  }

  const deleteTransaction = () => {
    database.expenseRepo
      .deleteExpense(existingData, { setLoading })
      .then(() => {
        reload()
        navigation.goBack()
      })
  }

  const updateTransaction = (data: UpdateTransaction) => {
    database.expenseRepo.updateExpense(data, { setLoading }).then(() => {
      reload()
      navigation.goBack()
    })
  }

  // ------------------------- Events -------------------------------------

  useMountEffect(() => {
    getAccounts()
    getLoan()
  }, [])

  // -----------------------------------------------------------------------

  const toggleLoan = () => {
    if (isLoan) {
      setValue(`loan`, '')
    } else {
      setValue(`account`, '')
    }
    setLoan(prev => !prev)
  }

  return (
    <ModelContainer
      label="Edit Expense"
      action={
        <Box>
          <ConfirmationButton
            bg="red.500"
            mb={5}
            label="Delete"
            disabled={isLoading}
            onApply={deleteTransaction}
          />
          <Button
            onPress={handleSubmit(updateTransaction)}
            isDisabled={isLoading}
          >
            Save
          </Button>
        </Box>
      }
    >
      <Box>
        <FormTextField name="name" label="Name" isRequired {...formProps} />
        <FormDatePicker isRequired name="date" label="Date" {...formProps} />

        {!isLoan ? (
          <FormSelectField
            name="account"
            label="Account"
            options={accountList}
            {...formProps}
            isDisabled={isLoading || isAccountLoading}
            isRequired
          />
        ) : (
          <FormSelectField
            name="loan"
            label="Loan"
            options={loanList}
            {...formProps}
            isDisabled={isLoading || isLoanLoading}
            isRequired
          />
        )}

        <FormNumberField
          name="transaction"
          label="Amount"
          keyboardType="numeric"
          isRequired
          isCurrency
          {...formProps}
        />

        <Box style={layout.lrPartition}>
          <FormRadioGroup
            name="type"
            options={processOptions}
            flexDirection="row"
            isRequired
            {...formProps}
          />

          <Box mt="-5" style={layout.asideRow}>
            <Switch isChecked={isLoan} onChange={toggleLoan} />
            Loan
          </Box>
        </Box>

        <FormTextArea name="description" label="Description" {...formProps} />
      </Box>
    </ModelContainer>
  )
}

export default UpdateExpense
