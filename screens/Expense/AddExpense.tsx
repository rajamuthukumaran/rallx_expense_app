import React, { useMemo, useState } from 'react'
import { Box, Button } from 'native-base'
import { useForm } from 'react-hook-form'
import { NativeStackScreenProps } from '@react-navigation/native-stack'
import { isEmpty, omit } from 'lodash'

import { ModelContainer } from '../../components/Layouts'
import { useDatabase } from '../../data'
import { Expense } from '../../data/entities/expense'
import { Transaction } from '../../data/interface/expense'
import {
  FormDatePicker,
  FormSelectField,
  FormTextArea,
  FormTextField,
} from '../../components/Form'
import { ExpenseType } from '../../data/constants/expense'
import { Account } from '../../data/entities/account'
import useMountEffect from '../../hooks/useMountEffect'
import { nullify } from '../../utils/commonFunctions'
import AccountSelector from './component/AccountSelector'
import { Loan } from '../../data/entities/loan'
import useBalanceAction from '../../GlobalValue/Actions/useBalanceAction'
import { Category } from '../../data/entities/category'

const AddExpense: React.FC<NativeStackScreenProps<any>> = ({
  navigation,
  route,
}) => {
  const database = useDatabase()
  const { reload } = useBalanceAction()

  const [accountList, setAccountList] = useState<Account[]>([])
  const [loanList, setLoanList] = useState<Loan[]>([])
  const [categories, setCategories] = useState<Category[]>([])

  // loaders
  const [isLoading, setLoading] = useState(false)
  const [isAccountLoading, setAccountLoading] = useState(false)
  const [isLoanLoading, setLoanLoading] = useState(false)
  const [isCategoriesLoading, setCategoriesLoading] = useState(false)

  const existingData: Expense = useMemo(() => {
    return route.params?.data
  }, [route.params])

  const isEdit = useMemo(() => {
    if (isEmpty(omit(existingData, 'date'))) {
      return false
    }
    return true
  }, [existingData])

  // ------------------ React hook form initaliztation -------------------
  const initData: Transaction = {
    name: existingData?.name || '',
    category: existingData?.category?.id || '',
    accounts: [
      {
        account: '',
        amount: undefined,
        process: ExpenseType.expense,
      },
    ],
    date: existingData?.date || new Date(),
    description: existingData?.description || '',
  }

  const expenseForm = useForm({
    defaultValues: initData,
  })

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = expenseForm

  const formProps = {
    control,
    errors,
    containerProps: { mb: 5 },
    isDisabled: isLoading,
  }

  // -------------------- Database ops -------------------------------------

  const getAccounts = () => {
    database.accountRepo
      .getAllAccounts({ setLoading: setAccountLoading })
      .then(res => setAccountList(res))
  }

  const getLoan = () => {
    database.loanRepo
      .getAllLoans({
        setLoading: setLoanLoading,
        canUseToPay: true,
      })
      .then(res => setLoanList(res))
  }

  const getCategories = () => {
    database.categoryRepo
      .getAllCategories({
        setLoading: setCategoriesLoading,
      })
      .then(res => {
        if (res?.length) {
          setCategories(res)
        }
      })
  }

  const addExpense = (data: Transaction) => {
    database.expenseRepo.addExpense(data, { setLoading }).then(() => {
      reload()
      navigation.goBack()
    })
  }

  // -----------------------------------------------------------------------

  const onSubmit = (data: Transaction) => {
    const formattedData = nullify(data)
    if (isEdit) {
      // updateExpense(formattedData)
    } else {
      addExpense(formattedData)
    }
  }

  // ------------------------- Events -------------------------------------

  useMountEffect(() => {
    getAccounts()
    getLoan()
    getCategories()
  }, [])

  // -----------------------------------------------------------------------

  const loaders = {
    isLoading,
    isAccountLoading,
    isLoanLoading,
  }

  return (
    <ModelContainer
      label="Add Expense"
      action={
        <Box>
          <Button onPress={handleSubmit(onSubmit)} isDisabled={isLoading}>
            Save
          </Button>
        </Box>
      }
    >
      <Box>
        <FormTextField name="name" label="Name" isRequired {...formProps} />
        <FormDatePicker isRequired name="date" label="Date" {...formProps} />

        <FormSelectField
          name="category"
          label="Category"
          options={categories}
          {...formProps}
          isDisabled={isLoading || isCategoriesLoading}
          isRequired
        />

        <AccountSelector
          accountList={accountList}
          loanList={loanList}
          expenseForm={expenseForm}
          formProps={formProps}
          loaders={loaders}
        />

        <FormTextArea name="description" label="Description" {...formProps} />
      </Box>
    </ModelContainer>
  )
}

export default AddExpense
