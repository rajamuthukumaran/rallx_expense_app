import { groupBy } from 'lodash'
import moment from 'moment'
import { Box } from 'native-base'
import React, { useMemo, useState } from 'react'
import { ListRenderItemInfo, VirtualizedList } from 'react-native'
import { Text } from '../../components/Input'
import MonthPicker from '../../components/Input/MonthPicker'
import { ScreenContainer } from '../../components/Layouts'
import { useDatabase } from '../../data'
import { Expense as IExpense } from '../../data/entities/expense'
import { TransactionBalance } from '../../data/interface/expense'
import useMountEffect from '../../hooks/useMountEffect'
import { layout } from '../../mixins'
import { currency } from '../../utils/commonFunctions'
import { MomentDate } from '../../utils/commonTypes'
import ExpenseCard from './component/ExpenseCard'

export type TransactionObj = {
  [key: string]: IExpense[]
}

const Expense: React.FC = (): React.ReactElement => {
  const database = useDatabase()

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [currentDate, setCurrentDate] = useState<MomentDate>(moment())
  const [transactions, setTransactions] = useState<TransactionObj>({})
  const [balance, setBalance] = useState<TransactionBalance>({
    income: 0,
    expense: 0,
    transfer: 0,
  })
  const [isLoading, setLoading] = useState(false)

  const days = useMemo(() => {
    return Object.keys(transactions || {})
  }, [transactions])

  // -------------------- Database ops -------------------------------------

  const getExpense = () => {
    database.expenseRepo
      .getExpense({
        setLoading,
        filter: {
          date: {
            startDate: moment(currentDate).startOf('M').toISOString(),
            endDate: moment(currentDate).endOf('M').toISOString(),
          },
        },
      })
      .then(res => {
        const formatted = groupBy(res, value => {
          return moment(value.date).format('DD MMM YYYY')
        })

        const sorted = {}
        Object.keys(formatted || {})
          .sort()
          .forEach(key => {
            sorted[key] = formatted[key]
          })

        setTransactions(sorted)
      })
  }

  const getBalance = async () => {
    const res = await database.expenseRepo.getBalance({
      date: {
        startDate: moment(currentDate).startOf('M').toISOString(),
        endDate: moment(currentDate).endOf('M').toISOString(),
      },
    })
    setBalance({
      income: res.income || 0,
      expense: res.expense || 0,
      transfer: res.transfer || 0,
    })
  }

  // ------------------------- Events -------------------------------------

  useMountEffect(() => {
    getExpense()
    getBalance()
  }, [currentDate])

  return (
    <ScreenContainer>
      {/* <Heading label={moment(currentDate).format('MMMM YY')} /> */}
      <MonthPicker value={currentDate} onChange={setCurrentDate} />
      <Box>
        <Box style={layout.lrPartition} my={3}>
          <Text color="green.500">{currency(balance.income)}</Text>
          <Text color="red.500">{currency(balance.expense)}</Text>
          <Text color="primary.200">
            {currency(balance.income - balance.expense)}
          </Text>
        </Box>

        {!isLoading && !!days?.length && (
          <Box mb="130">
            <VirtualizedList
              data={days}
              initialNumToRender={3}
              getItemCount={value => value?.length}
              getItem={(item, index) => item?.[index]}
              keyExtractor={item => item}
              renderItem={(vProps: ListRenderItemInfo<string>) => (
                <ExpenseCard
                  data={transactions[vProps.item]}
                  date={vProps.item}
                />
              )}
            />
          </Box>
        )}
      </Box>
    </ScreenContainer>
  )
}

export default Expense
