import React, { useEffect, useState } from 'react'
import { Box, IconButton, Switch, Text } from 'native-base'
import {
  Control,
  FieldErrors,
  useFieldArray,
  UseFormReturn,
} from 'react-hook-form'
import { MaterialIcons } from '@expo/vector-icons'

import { Transaction } from '../../../data/interface/expense'
import { Account } from '../../../data/entities/account'
import {
  FormNumberField,
  FormRadioGroup,
  FormSelectField,
} from '../../../components/Form'
import { CustomButton } from '../../../components/Input/Button'
import { ExpenseType } from '../../../data/constants/expense'
import { RadioOption } from '../../../components/Input/RadioGroup'
import Label from '../../../components/Label'
import { layout } from '../../../mixins'
import { Loan } from '../../../data/entities/loan'

export type AccountSectionProps = {
  formProps: {
    control: Control<any>
    errors: FieldErrors<Transaction>
    containerProps: any
    isDisabled: boolean
  }
  expenseForm: UseFormReturn<any, Record<string, unknown>>
  accountList: Account[]
  loanList: Loan[]
  loaders: {
    isLoading: boolean
    isAccountLoading: boolean
    isLoanLoading: boolean
  }
}

export const processOptions: RadioOption[] = [
  {
    label: 'Credit',
    value: ExpenseType.income,
  },
  {
    label: 'Debit',
    value: ExpenseType.expense,
  },
]

const AccountSelector: React.FC<AccountSectionProps> = props => {
  const { expenseForm } = props || {}

  // ------------------ React hook form initaliztation -------------------

  const { control, getValues } = expenseForm

  const { fields, remove, append } = useFieldArray({
    name: 'accounts',
    control,
  })

  // -------------------- Array manupulation -------------------------------

  const addAccountField = () => {
    append({
      account: '',
      amount: undefined,
      process: ExpenseType.expense,
    })
  }

  const removeAccountField = (index: number) => {
    const currentSize = getValues('accounts')
    if (currentSize?.length > 1) {
      remove(index)
    }
  }

  // -----------------------------------------------------------------------

  return (
    <Box>
      <Label isRequired>Transactions</Label>
      <Box>
        {fields?.map((field, index) => (
          <TransactionEntry
            {...props}
            // eslint-disable-next-line react/no-array-index-key
            key={index}
            index={index}
            removeAccountField={removeAccountField}
          />
        ))}
      </Box>
      <Box mb={4}>
        <CustomButton onPress={addAccountField}>
          <Text color="primary.200">+ Add account</Text>
        </CustomButton>
      </Box>
    </Box>
  )
}

const TransactionEntry: React.FC<
  {
    index: number
    removeAccountField: (index: number) => void
  } & AccountSectionProps
> = React.memo(
  ({
    expenseForm,
    formProps,
    accountList,
    loanList,
    loaders,
    index,
    removeAccountField,
  }) => {
    const { setValue } = expenseForm
    const path = `accounts.[${index}]`

    const [isLoan, setLoan] = useState(false)

    const toggleLoan = () => {
      if (isLoan) {
        setValue(`${path}.loan`, '')
      } else {
        setValue(`${path}.account`, '')
      }
      setLoan(prev => !prev)
    }

    useEffect(() => {
      if (isLoan && loanList?.length === 1) {
        setValue(`${path}.loan`, loanList[0]?.id)
      } else if (!isLoan && accountList?.length === 1) {
        setValue(`${path}.account`, loanList[0]?.id)
      }
      // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [isLoan])

    return (
      <Box
        // eslint-disable-next-line react/no-array-index-key
        key={index}
        borderRadius={25}
        my={4}
        px={4}
        pt={4}
        bg="black.200"
      >
        <Box flexWrap="wrap" flexDirection="row-reverse" mb="-5">
          <IconButton
            variant="ghost"
            _icon={{
              as: MaterialIcons,
              name: 'close',
              color: 'white.100',
              size: 6,
            }}
            borderRadius={50}
            onPress={() => removeAccountField(index)}
          />
        </Box>

        {!isLoan ? (
          <FormSelectField
            name={`${path}.account`}
            label="Account"
            options={accountList}
            {...formProps}
            isDisabled={loaders.isLoading || loaders.isAccountLoading}
            isRequired
          />
        ) : (
          <FormSelectField
            name={`${path}.loan`}
            label="Loan"
            options={loanList}
            {...formProps}
            isDisabled={loaders.isLoading || loaders.isLoanLoading}
            isRequired
          />
        )}

        <FormNumberField
          name={`${path}.amount`}
          label="Amount"
          keyboardType="numeric"
          isRequired
          isCurrency
          {...formProps}
        />

        <Box style={layout.lrPartition}>
          <FormRadioGroup
            name={`${path}.process`}
            options={processOptions}
            flexDirection="row"
            isRequired
            {...formProps}
          />

          <Box mt="-5" style={layout.asideRow}>
            <Switch isChecked={isLoan} onChange={toggleLoan} />
            Loan
          </Box>
        </Box>
      </Box>
    )
  },
)

export default AccountSelector
