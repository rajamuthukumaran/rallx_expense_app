import React, { useMemo } from 'react'
import { Pressable } from 'react-native'
import { Box } from 'native-base'
import { groupBy } from 'lodash'
import moment from 'moment'

import { useNavigation } from '@react-navigation/core'
import { Expense } from '../../../data/entities/expense'
import { Text } from '../../../components/Input'
import { RoundButton } from '../../../components/Input/Button'
import { fonts, layout } from '../../../mixins'
import { currency } from '../../../utils/commonFunctions'
import { TransactionObj } from '..'
import { routes } from '../../../config/routes'

export type ExpenseCardProps = {
  data: Expense[]
  date: string
}

const ExpenseCard: React.FC<ExpenseCardProps> = ({ data, date }) => {
  const navigation: any = useNavigation()

  const balance = useMemo(() => {
    const { income, expense } = data.reduce(
      (acc, cur) => {
        return {
          ...acc,
          [cur.type]: (acc?.[cur.type] || 0) + cur.transaction,
        }
      },
      { income: 0, expense: 0 },
    )

    return income - expense
  }, [data])

  const formattedExp: TransactionObj = useMemo(() => {
    return groupBy(data, value => value?.category?.name || 'Other')
  }, [data])

  const categories = useMemo(() => {
    return Object.keys(formattedExp || {})
  }, [formattedExp])

  // -----------------------------------------------------------------------

  const openAddExpense = (entry?: Expense) => {
    navigation.navigate(`${routes.expense[entry ? 'edit' : 'add']}`, {
      data: entry
        ? {
            ...entry,
          }
        : {
            date: moment(date, 'DD MMM YYYY').toISOString(),
          },
    })
  }

  return (
    <Box bg="black.50" px="5" py="3" borderRadius="15" mb="5">
      <Box style={layout.lrPartition}>
        <Box style={layout.asideRow}>
          <Text fontSize="xl" fontFamily={fonts.Montserrat500}>
            {moment(date, 'DD MMM YYYY').format('DD dddd')}
          </Text>
          <RoundButton ml="4" size={22} onPress={() => openAddExpense()} />
        </Box>

        <Text color={balance > 0 ? 'green.500' : 'red.500'}>
          {currency(balance)}
        </Text>
      </Box>

      <Box>
        {categories.map(cat => (
          <Box key={cat} mt="3">
            <Text
              color="primary.200"
              fontSize="md"
              fontFamily={fonts.Montserrat700}
              mb="3"
            >
              {cat}
            </Text>
            {formattedExp[cat]?.map(exp => (
              <Pressable key={exp.id} onPress={() => openAddExpense(exp)}>
                <Box mb="2">
                  <Box style={layout.lrPartition}>
                    <Text numberOfLines={1} w={190} ellipsizeMode="tail">
                      {exp.name}
                    </Text>
                    <Text
                      color={exp.type === 'income' ? 'green.500' : 'red.500'}
                    >
                      {currency(exp.transaction)}
                    </Text>
                  </Box>
                  <Text fontSize="xs" color="primary.200">
                    {exp?.account?.name || exp?.loan?.name}
                  </Text>
                </Box>
              </Pressable>
            ))}
          </Box>
        ))}
      </Box>
    </Box>
  )
}

export default ExpenseCard
