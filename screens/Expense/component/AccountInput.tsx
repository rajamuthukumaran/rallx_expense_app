import React, { useMemo } from 'react'
import { Box } from 'native-base'
import {
  FormNumberField,
  FormTextField,
  FormTextFieldProps,
} from '../../../components/Form'

export type AccountInputProps = FormTextFieldProps

const AccountInput: React.FC<AccountInputProps> = ({ name, ...rest }) => {
  const { accountId, amount } = useMemo(() => {
    return {
      accountId: `${name}.accountId`,
      amount: `${name}.amount`,
    }
  }, [name])

  return (
    <Box>
      <Box>
        <FormTextField name={accountId} isReadOnly {...rest} />
        <FormNumberField name={amount} {...rest} />
      </Box>
    </Box>
  )
}

export default AccountInput
